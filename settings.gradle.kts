enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

rootProject.name = "stok"

pluginManagement {
    val nexusUser = System.getProperty("stok.nexus.user")
    val nexusPassword = System.getProperty("stok.nexus.password")

    repositories {
        gradlePluginPortal()
        mavenCentral()
        google()
        maven {
            name = "stok-hosted-maven-development"
            url = uri("https://nexus.stok.dev/repository/stok-hosted-maven-development/")
            credentials {
                username = nexusUser
                password = nexusPassword
            }
        }
    }
}

dependencyResolutionManagement {
    val nexusUser = System.getProperty("stok.nexus.user")
    val nexusPassword = System.getProperty("stok.nexus.password")

    @Suppress("UnstableApiUsage")
    repositories {
        mavenCentral()
        google()
        maven {
            name = "stok-hosted-maven-development"
            url = uri("https://nexus.stok.dev/repository/stok-hosted-maven-development/")
            credentials {
                username = nexusUser
                password = nexusPassword
            }
        }
    }
}

plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version("0.8.0")
}

// Include subprojects
fileTree(rootProject.projectDir) {
    include("**/build.gradle.kts")
    exclude("build.gradle.kts") // Exclude root build.gradle.kts
    exclude("**/buildSrc") // Exclude build sources
    exclude(".*") // Exclude hidden sources
    exclude("**/build", "**/out") // Exclude build directories
}
    .asSequence()
    .map(File::getParent) // Resolve directory of found build.gradle.kts file
    .map(::relativePath).map { relativePath -> relativePath.replace(File.separator, ":") }
    .asIterable().let(::include)
