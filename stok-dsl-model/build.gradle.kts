plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                api(projects.stokDomainModel)
                api(projects.stokIntegrationModel)
            }
        }
    }
}
