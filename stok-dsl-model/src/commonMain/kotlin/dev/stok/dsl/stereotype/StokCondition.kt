package dev.stok.dsl.stereotype

import dev.stok.common.util.uncheckedCast
import dev.stok.dsl.context.StrategyContext
import dev.stok.stereotype.Computation

fun interface StokCondition : Computation<StrategyContext, Boolean> {

    fun isSatisfied(context: StrategyContext): Boolean

    context(StrategyContext) override suspend fun compute(): Boolean = isSatisfied(uncheckedCast(this))
}
