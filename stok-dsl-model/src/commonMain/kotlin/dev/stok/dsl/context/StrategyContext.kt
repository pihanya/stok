package dev.stok.dsl.context

import dev.stok.StokStrategyManager
import dev.stok.common.definition.LibraryApi
import dev.stok.dsl.model.BrokerDelegate
import dev.stok.dsl.model.IndicatorsDelegate
import dev.stok.dsl.model.InstrumentsDelegate
import dev.stok.dsl.model.PortfolioDelegate
import dev.stok.dsl.model.QuotesDelegate
import dev.stok.dsl.stereotype.StrategyDslMarker
import kotlinx.datetime.Instant

@[StrategyDslMarker LibraryApi]
open class StrategyContext(
    currentTime: Instant,
    override val strategyManager: StokStrategyManager
) : BootstrapContext(currentTime, strategyManager.bootstrap), StrategyManagerAwareContext {

    val portfolio: PortfolioDelegate by lazy { PortfolioDelegate(bootstrap) }

    val broker: BrokerDelegate by lazy { BrokerDelegate(bootstrap) }

    val indicators: IndicatorsDelegate by lazy { IndicatorsDelegate(bootstrap) }

    val instruments: InstrumentsDelegate by lazy { InstrumentsDelegate(bootstrap) }

    val quotes: QuotesDelegate by lazy { QuotesDelegate(bootstrap) }
}
