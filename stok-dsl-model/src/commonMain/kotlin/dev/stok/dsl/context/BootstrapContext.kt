package dev.stok.dsl.context

import dev.stok.StokBootstrap
import dev.stok.context.BootstrapAwareContext
import dev.stok.context.LoggerAwareContext
import dev.stok.context.ParametersAwareContext
import dev.stok.context.TimeAwareContext
import dev.stok.context.VariablesAwareContext
import dev.stok.stereotype.MutableNamedValueContainer
import dev.stok.stereotype.NamedValueContainer
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.datetime.Instant

open class BootstrapContext(
    final override val currentTime: Instant,
    final override val bootstrap: StokBootstrap
) : ParametersAwareContext, VariablesAwareContext, BootstrapAwareContext, TimeAwareContext, LoggerAwareContext {

    override val logger: KLogger by bootstrap::logger

    override val parameters: NamedValueContainer<Any> by bootstrap::parameters

    override val variables: MutableNamedValueContainer<Any> by bootstrap::variables

    companion object {

        private val LOGGER: KLogger = KotlinLogging.logger {}
    }
}
