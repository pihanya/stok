package dev.stok.dsl.context

import dev.stok.common.definition.LibraryApi
import dev.stok.dsl.stereotype.DslAction
import dev.stok.dsl.stereotype.StokCondition
import dev.stok.dsl.stereotype.StrategyDslMarker
import dev.stok.dsl.typing.PeriodicMoment
import dev.stok.model.TriggerRegistration
import dev.stok.stereotype.StokContext
import dev.stok.stereotype.StokEvent
import kotlin.reflect.KClass
import kotlin.time.Duration

@[StrategyDslMarker LibraryApi]
interface StrategyBuilderContext : StokContext {

    fun lifecycle(closure: LifecycleBuilderContext.() -> Unit)

    fun everyPeriod(
        period: Duration,
        triggerOnStart: Boolean = true,
        action: DslAction<StrategyContext>
    ): TriggerRegistration

    fun atMoment(moment: PeriodicMoment, action: DslAction<StrategyContext>): TriggerRegistration

    fun onCondition(condition: StokCondition, action: DslAction<StrategyContext>): TriggerRegistration

    fun <E : StokEvent> onEvent(
        eventClass: KClass<E>,
        action: DslAction<StrategyEventContext<E>>
    ): TriggerRegistration
}
