package dev.stok.dsl.context

import dev.stok.common.definition.LibraryApi
import dev.stok.dsl.stereotype.DslAction
import dev.stok.dsl.stereotype.LifecycleDslMarker
import dev.stok.dsl.stereotype.StrategyDslMarker
import dev.stok.integration.event.LifecycleEvent.AfterStateChangeEvent
import dev.stok.integration.event.LifecycleEvent.BeforeStateChangeEvent
import dev.stok.model.TriggerRegistration
import dev.stok.stereotype.StokContext

@Suppress("TooManyFunctions")
@[StrategyDslMarker LifecycleDslMarker LibraryApi]
interface LifecycleBuilderContext : StokContext {

    fun beforeStart(action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>): TriggerRegistration

    fun afterStart(action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>): TriggerRegistration

    fun beforePause(action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>): TriggerRegistration

    fun afterPause(action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>): TriggerRegistration

    fun beforeResume(action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>): TriggerRegistration

    fun afterResume(action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>): TriggerRegistration

    fun beforeInterrupt(action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>): TriggerRegistration

    fun afterInterrupt(action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>): TriggerRegistration

    fun beforeFinish(action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>): TriggerRegistration

    fun afterFinish(action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>): TriggerRegistration
}
