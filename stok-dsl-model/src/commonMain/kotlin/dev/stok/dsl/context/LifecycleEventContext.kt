package dev.stok.dsl.context

import dev.stok.StokBootstrap
import dev.stok.context.EventAwareContext
import dev.stok.dsl.stereotype.LifecycleDslMarker
import dev.stok.dsl.stereotype.StrategyDslMarker
import dev.stok.integration.event.LifecycleEvent
import kotlinx.datetime.Instant

@[StrategyDslMarker LifecycleDslMarker]
class LifecycleEventContext<out E : LifecycleEvent>(
    currentTime: Instant,
    bootstrap: StokBootstrap,
    override val event: E
) : BootstrapContext(currentTime, bootstrap), EventAwareContext
