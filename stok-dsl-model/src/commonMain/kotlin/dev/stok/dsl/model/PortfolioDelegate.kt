package dev.stok.dsl.model

import dev.stok.StokBootstrap
import dev.stok.dsl.kotlin.extension.accountApi
import dev.stok.dsl.kotlin.extension.portfolioApi
import dev.stok.integration.api.AccountApi
import dev.stok.integration.api.PortfolioApi
import dev.stok.model.StokAccount
import dev.stok.model.StokPosition
import dev.stok.typing.CurrencyCode
import dev.stok.typing.MoneyValue

class PortfolioDelegate(private val bootstrap: StokBootstrap) {

    internal val accountApi: AccountApi by bootstrap::accountApi

    internal val portfolioApi: PortfolioApi by bootstrap::portfolioApi

    suspend fun getAllMoney(account: StokAccount): List<MoneyValue> =
        portfolioApi.getAllMoney(account)

    suspend fun getMoney(currency: CurrencyCode, account: StokAccount): MoneyValue =
        portfolioApi.getMoney(currency, account)

    suspend fun getAccounts(): List<StokAccount> =
        accountApi.getAccounts()

    suspend fun getPositions(account: StokAccount): List<StokPosition> =
        portfolioApi.getPositions(account)
}
