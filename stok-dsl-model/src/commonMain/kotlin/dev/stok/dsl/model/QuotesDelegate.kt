package dev.stok.dsl.model

import dev.stok.OrderBookDepths
import dev.stok.StokBootstrap
import dev.stok.dsl.kotlin.extension.quoteApi
import dev.stok.integration.api.QuoteApi
import dev.stok.integration.model.OrderBook
import dev.stok.typing.InstrumentId

class QuotesDelegate(private val bootstrap: StokBootstrap) {

    internal val quoteApi: QuoteApi by bootstrap::quoteApi

    suspend fun getOrderBook(
        instrumentId: InstrumentId,
        depth: Short = OrderBookDepths.DEPTH_50
    ): OrderBook = quoteApi.getOrderBook(instrumentId = instrumentId, depth = depth)
}
