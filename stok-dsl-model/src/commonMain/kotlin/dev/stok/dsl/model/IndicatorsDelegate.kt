package dev.stok.dsl.model

import dev.stok.StokBootstrap
import dev.stok.common.definition.InternalLibraryApi
import dev.stok.dsl.kotlin.extension.quoteApi
import dev.stok.integration.api.QuoteApi

class IndicatorsDelegate(private val bootstrap: StokBootstrap) {

    @InternalLibraryApi
    val quoteApi: QuoteApi by bootstrap::quoteApi
}
