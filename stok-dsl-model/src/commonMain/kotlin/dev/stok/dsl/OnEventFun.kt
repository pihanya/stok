package dev.stok.dsl

import dev.stok.common.definition.LibraryApi
import dev.stok.dsl.context.StrategyBuilderContext
import dev.stok.dsl.context.StrategyEventContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.model.TriggerRegistration
import dev.stok.stereotype.StokEvent

@LibraryApi
inline fun <reified E : StokEvent> StrategyBuilderContext.onEvent(
    action: DslAction<StrategyEventContext<E>>
): TriggerRegistration =
    onEvent(eventClass = E::class, action = action)
