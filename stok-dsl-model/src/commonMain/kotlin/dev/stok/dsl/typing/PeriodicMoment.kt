package dev.stok.dsl.typing

import kotlinx.datetime.Instant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.DurationUnit
import kotlin.time.toDuration

private const val SECONDS_IN_DAY: Long = 86400
private const val SECONDS_IN_HOUR: Long = 3600
private const val SECONDS_IN_MINUTE: Long = 60

sealed interface PeriodicMoment {

    val period: Duration

    fun computeUntilNextActivation(currentTime: Instant): Duration

    fun computeSincePreviousActivation(currentTime: Instant): Duration =
        period - computeUntilNextActivation(currentTime)

    companion object {

        fun daily(atHour: Int = 0, atMinute: Int = 0, atSecond: Int = 0): PeriodicMoment =
            DayPeriodicMoment(atHour, atMinute, atSecond)

        fun hourly(atMinute: Int = 0, atSecond: Int = 0): PeriodicMoment =
            HourPeriodicMoment(atMinute, atSecond)

        fun minutely(atSecond: Int = 0): PeriodicMoment =
            MinutePeriodicMoment(atSecond)
    }
}

private data class DayPeriodicMoment(
    val atHour: Int,
    val atMinute: Int,
    val atSecond: Int,
    val atNano: Int = 0
) : PeriodicMoment {

    override val period: Duration = 1.days

    override fun computeUntilNextActivation(currentTime: Instant): Duration {
        val timeSecondsDuration = (currentTime.epochSeconds % SECONDS_IN_DAY).toDuration(DurationUnit.SECONDS)
        val timeNanosDuration = currentTime.nanosecondsOfSecond.toDuration(DurationUnit.NANOSECONDS)

        val momentSecondsDuration = 0
            .plus(atHour * SECONDS_IN_HOUR)
            .plus(atMinute * SECONDS_IN_MINUTE)
            .plus(atSecond)
            .toDuration(DurationUnit.SECONDS)
        val momentNanosDuration = atNano.toDuration(DurationUnit.NANOSECONDS)

        val duration = (momentSecondsDuration - timeSecondsDuration) + (momentNanosDuration - timeNanosDuration)
        return if (duration >= Duration.ZERO) duration else 1.days + duration
    }

    override fun toString(): String = "DayPeriodicMoment(atHour=$atHour, atMinute=$atMinute, atSecond=$atSecond)"
}

private data class HourPeriodicMoment(
    val atMinute: Int,
    val atSecond: Int,
    val atNano: Int = 0
) : PeriodicMoment {

    override val period: Duration = 1.hours

    override fun computeUntilNextActivation(currentTime: Instant): Duration {
        val timeSecondsDuration = (currentTime.epochSeconds % SECONDS_IN_HOUR).toDuration(DurationUnit.SECONDS)
        val timeNanosDuration = currentTime.nanosecondsOfSecond.toDuration(DurationUnit.NANOSECONDS)

        val momentSecondsDuration = 0
            .plus(atMinute * SECONDS_IN_MINUTE)
            .plus(atSecond)
            .toDuration(DurationUnit.SECONDS)
        val momentNanosDuration = atNano.toDuration(DurationUnit.NANOSECONDS)

        val duration = (momentSecondsDuration - timeSecondsDuration) + (momentNanosDuration - timeNanosDuration)
        return if (duration >= Duration.ZERO) duration else 1.hours + duration
    }

    override fun toString(): String = "HourPeriodicMoment(atMinute=$atMinute, atSecond=$atSecond)"
}

private data class MinutePeriodicMoment(
    val atSecond: Int,
    val atNano: Int = 0
) : PeriodicMoment {

    override val period: Duration = 1.minutes

    override fun computeUntilNextActivation(currentTime: Instant): Duration {
        val timeSecondsDuration = (currentTime.epochSeconds % SECONDS_IN_MINUTE).toDuration(DurationUnit.SECONDS)
        val timeNanosDuration = currentTime.nanosecondsOfSecond.toDuration(DurationUnit.NANOSECONDS)

        val momentSecondsDuration = atSecond.toDuration(DurationUnit.SECONDS)
        val momentNanosDuration = atNano.toDuration(DurationUnit.NANOSECONDS)

        val duration = (momentSecondsDuration - timeSecondsDuration) + (momentNanosDuration - timeNanosDuration)
        return if (duration >= Duration.ZERO) duration else 1.minutes + duration
    }

    override fun toString(): String = "MinutePeriodicMoment(atSecond=$atSecond)"
}
