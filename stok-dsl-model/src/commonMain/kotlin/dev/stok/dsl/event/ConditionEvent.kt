package dev.stok.dsl.event

import dev.stok.dsl.stereotype.StokCondition
import dev.stok.stereotype.StokEvent

interface ConditionEvent : StokEvent {

    val condition: StokCondition
}
