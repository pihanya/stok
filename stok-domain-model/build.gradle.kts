plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                api(projects.stokCommons)
                implementation(libs.kotlin.reflect)
            }
        }
    }
}
