package dev.stok.enum

/**
 * See [StrategyState PlantUML state diagram](https://gitlab.com/stok-solutions/stok/-/snippets/2559912)
 *
 */
enum class StrategyState {

    CREATED,

    SCHEDULED,

    PARKED,

    RUNNING,

    PAUSING,

    PAUSED,

    FINISHING,

    FINISHED
}
