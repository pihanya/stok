package dev.stok.context

import dev.stok.stereotype.NamedValueContainer
import dev.stok.stereotype.StokContext

interface ParametersAwareContext : StokContext {

    val parameters: NamedValueContainer<Any>
}
