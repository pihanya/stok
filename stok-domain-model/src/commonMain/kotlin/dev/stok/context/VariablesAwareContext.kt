package dev.stok.context

import dev.stok.stereotype.MutableNamedValueContainer
import dev.stok.stereotype.StokContext

interface VariablesAwareContext : StokContext {

    val variables: MutableNamedValueContainer<Any>
}
