package dev.stok.context

import dev.stok.stereotype.StokContext
import kotlinx.datetime.Instant

interface TimeAwareContext : StokContext {

    val currentTime: Instant
}
