package dev.stok.context

import dev.stok.stereotype.StokContext
import io.github.oshai.kotlinlogging.KLogger

interface LoggerAwareContext : StokContext {

    val logger: KLogger
}
