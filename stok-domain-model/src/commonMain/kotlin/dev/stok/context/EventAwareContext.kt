package dev.stok.context

import dev.stok.stereotype.StokContext
import dev.stok.stereotype.StokEvent

interface EventAwareContext : StokContext {

    val event: StokEvent
}
