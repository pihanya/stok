package dev.stok.context

import dev.stok.StokBootstrap
import dev.stok.stereotype.StokContext

interface BootstrapAwareContext : StokContext {

    val bootstrap: StokBootstrap
}
