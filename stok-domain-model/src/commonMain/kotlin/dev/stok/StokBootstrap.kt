package dev.stok

import dev.stok.common.definition.LibraryApi
import dev.stok.stereotype.MutableNamedValueContainer
import dev.stok.stereotype.NamedValueContainer
import dev.stok.stereotype.StatefulElement
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.datetime.Clock

@LibraryApi
interface StokBootstrap : NamedValueContainer<Any>, StatefulElement<StokBootstrap> {

    val logger: KLogger

    val clock: Clock

    val parameters: NamedValueContainer<Any>

    val variables: MutableNamedValueContainer<Any>

    companion object {

        val LOGGER: KLogger = KotlinLogging.logger {}
    }
}
