package dev.stok.typing

/**
 * @see dev.stok.InstrumentTypes
 */
typealias InstrumentType = String
