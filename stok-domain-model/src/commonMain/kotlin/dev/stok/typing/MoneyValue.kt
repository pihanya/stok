package dev.stok.typing

import dev.stok.common.typing.DecimalValue

data class MoneyValue(
    val amount: DecimalValue,
    val currency: CurrencyCode
)
