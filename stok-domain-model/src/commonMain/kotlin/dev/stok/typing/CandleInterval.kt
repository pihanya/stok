package dev.stok.typing

/**
 * @see dev.stok.CandleIntervals
 */
typealias CandleInterval = kotlin.time.Duration
