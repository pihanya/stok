package dev.stok

object OrderBookDepths {

    const val DEPTH_10: Short = 10

    const val DEPTH_20: Short = 20

    const val DEPTH_30: Short = 30

    const val DEPTH_40: Short = 40

    const val DEPTH_50: Short = 50
}
