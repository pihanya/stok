package dev.stok

import dev.stok.typing.CandleInterval
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

object CandleIntervals {

    val INTERVAL_1MIN: CandleInterval = 1.minutes

    val INTERVAL_2MIN: CandleInterval = 2.minutes

    val INTERVAL_3MIN: CandleInterval = 3.minutes

    val INTERVAL_5MIN: CandleInterval = 5.minutes

    val INTERVAL_10MIN: CandleInterval = 10.minutes

    val INTERVAL_15MIN: CandleInterval = 15.minutes

    val INTERVAL_30MIN: CandleInterval = 30.minutes

    val INTERVAL_1HOUR: CandleInterval = 1.hours

    val INTERVAL_2HOUR: CandleInterval = 2.hours

    val INTERVAL_4HOUR: CandleInterval = 4.hours

    val INTERVAL_1DAY: CandleInterval = 1.days

    val INTERVAL_1WEEK: CandleInterval = 7.days

    val INTERVAL_1MONTH: CandleInterval = 30.days

    private val values: List<CandleInterval> = listOf(
        INTERVAL_1MIN,
        INTERVAL_2MIN,
        INTERVAL_3MIN,
        INTERVAL_5MIN,
        INTERVAL_10MIN,
        INTERVAL_15MIN,
        INTERVAL_30MIN,
        INTERVAL_1HOUR,
        INTERVAL_2HOUR,
        INTERVAL_4HOUR,
        INTERVAL_1DAY,
        INTERVAL_1WEEK,
        INTERVAL_1MONTH,
    )

    fun isStandard(value: CandleInterval): Boolean =
        value in values
}
