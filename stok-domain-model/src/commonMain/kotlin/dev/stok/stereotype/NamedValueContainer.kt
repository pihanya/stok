package dev.stok.stereotype

interface NamedValueContainer<T> {

    fun hasByName(name: String): Boolean

    fun <V : T> getByName(name: String): V

    operator fun <V : T> get(name: String): V = getByName(name)

    operator fun contains(name: String): Boolean = hasByName(name)

    fun toMap(): Map<String, T>
}
