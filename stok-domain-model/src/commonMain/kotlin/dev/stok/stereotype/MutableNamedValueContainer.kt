package dev.stok.stereotype

interface MutableNamedValueContainer<T> : NamedValueContainer<T> {

    fun <V : T> setByName(name: String, value: V)

    operator fun <V : T> set(name: String, value: V) = setByName(name, value)
}
