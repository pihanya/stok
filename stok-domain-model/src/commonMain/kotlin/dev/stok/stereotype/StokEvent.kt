package dev.stok.stereotype

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.common.definition.LibraryApi
import kotlinx.datetime.Instant

@LibraryApi
interface StokEvent {

    var creationTime: Instant
        @InternalLibraryApi set

    var dispatchTime: Instant
        @InternalLibraryApi set
}
