package dev.stok.model

import dev.stok.typing.InstrumentId
import dev.stok.typing.InstrumentType
import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity

data class StokPosition(
    val id: String,
    val instrumentId: InstrumentId,
    val instrumentType: InstrumentType,
    val lots: Quantity,
    val averagePrice: MoneyValue
)
