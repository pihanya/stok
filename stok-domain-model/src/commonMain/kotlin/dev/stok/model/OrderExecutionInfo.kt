package dev.stok.model

import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity
import kotlinx.datetime.Instant

data class OrderExecutionInfo(
    val lotsExecuted: Quantity,
    val executedCommission: MoneyValue,
    val executionDate: Instant?
)
