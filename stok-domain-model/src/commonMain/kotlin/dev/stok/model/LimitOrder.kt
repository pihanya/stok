package dev.stok.model

import dev.stok.enum.TradeDirection
import dev.stok.typing.InstrumentId
import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity

class LimitOrder(
    direction: TradeDirection,
    instrumentId: InstrumentId,
    lots: Quantity,
    val price: MoneyValue,
    orderInfo: OrderInfo,
    executionInfo: OrderExecutionInfo
) : StokOrder(direction, instrumentId, lots, orderInfo, executionInfo)
