package dev.stok

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.common.definition.LibraryApi
import dev.stok.enum.StrategyState
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging

@LibraryApi
interface StokStrategyManager {

    val state: StrategyState

    @InternalLibraryApi val strategy: StokStrategy

    @InternalLibraryApi val bootstrap: StokBootstrap

    fun start(wait: Boolean = false)

    fun pause(wait: Boolean = false)

    fun resume(wait: Boolean = false)

    fun shutdown(wait: Boolean = false)

    companion object {

        val LOGGER: KLogger = KotlinLogging.logger {}
    }
}
