package dev.stok

import dev.stok.typing.InstrumentType

object InstrumentTypes {

    const val bond: InstrumentType = "bond"

    const val share: InstrumentType = "share"

    const val currency: InstrumentType = "currency"

    const val etf: InstrumentType = "etf"

    const val futures: InstrumentType = "futures"
}
