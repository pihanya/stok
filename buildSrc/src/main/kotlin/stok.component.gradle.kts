import com.diffplug.spotless.LineEnding
import dev.stok.gradle.kotlin.extension.javaVersion
import dev.stok.gradle.kotlin.extension.versionCatalog
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.nio.charset.Charset

plugins {
    id("stok.base")
    id("com.diffplug.spotless")
    id("kotlin-multiplatform")
    id("io.kotest.multiplatform")
}

kotlin {
    jvm {
        jvmToolchain(javaVersion.get().getMajorVersion().toInt())
        withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }

//    js(IR) {
//        browser {
//            commonWebpackConfig {
//                cssSupport {
//                    enabled.set(true)
//                }
//            }
//        }
//    }

//    linuxX64()

    val hostOs = System.getProperty("os.name")
    val isLinuxHost = (hostOs == "Linux")
    val isMacosHost = (hostOs == "Mac OS X")
    val isWindowsHost = hostOs.startsWith("Windows")
//    when {
//        // isLinuxHost -> linuxX64()
//        isMacosHost -> macosX64()
//        isWindowsHost -> mingwX64()
//    }

    sourceSets {
        val libs = versionCatalog.get()

        val commonMain by getting {
            dependencies {
                api(libs.kotlinx.coroutinesCore)
                api(libs.kotlinx.datetime)
                api(libs.kotlinLogging)
            }
        }
        val commonTest by getting {
            dependencies {
                api(libs.kotlin.test)
                api(libs.kotlin.test.annotationsCommon)
                api(libs.kotest.assertionsCore)
                api(libs.kotest.property)
                api(libs.kotest.frameworkDatatest)
                api(libs.kotest.frameworkApi)
                api(libs.kotest.frameworkEngine)
            }
        }

//        val nativeMain by creating
//        val nativeTest by creating
//        nativeMain.dependsOn(commonMain)
//        nativeTest.dependsOn(commonTest)

//        val linuxX64Main by getting
//        val linuxX64Test by getting
//        linuxX64Main.dependsOn(nativeMain)
//        linuxX64Test.dependsOn(nativeTest)

        if (isMacosHost) {
//            val macosX64Main by getting
//            val macosX64Test by getting
//            macosX64Main.dependsOn(nativeMain)
//            macosX64Test.dependsOn(nativeTest)
        }

        if (isWindowsHost) {
//            val mingwX64Main by getting
//            val mingwX64Test by getting
//            mingwX64Main.dependsOn(nativeMain)
//            mingwX64Test.dependsOn(nativeTest)
        }

        val jvmMain by getting
        val jvmTest by getting {
            dependencies {
                implementation(libs.kotest.runnerJunit5)
                implementation(libs.kotlin.test.junit5)
            }
        }

//        val jsMain by getting
//        val jsTest by getting {
//            dependencies {
//                implementation(libs.kotlin.test.js)
//            }
//        }
    }
}

kotlin {
    sourceSets {
        all {
            languageSettings {
                enableLanguageFeature("ContextReceivers")
                optIn("kotlin.time.ExperimentalTime")
                optIn("kotlinx.coroutines.DelicateCoroutinesApi")
                optIn("kotlinx.serialization.ExperimentalSerializationApi")
                optIn("kotlin.io.encoding.ExperimentalEncodingApi")
            }
        }
    }
}

// FIXME: As Kotest doesn't support nested tests in JS, so disabling them (https://github.com/kotest/kotest/pull/2957)
//tasks.named("jsTest").configure { enabled = false }
//tasks.named("jsBrowserTest").configure { enabled = false }

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = javaVersion.get().majorVersion
    }
}

extensions.configure<com.diffplug.gradle.spotless.SpotlessExtension> {
    kotlin {
        target("**/*.kt")
        ktlint(versionCatalog.get().versions.gradlePlugins.ktlint.requiredVersion)
            .editorConfigOverride(dev.stok.gradle.internal.ktlintEditorConfig)
        lineEndings = LineEnding.UNIX
        encoding = Charset.forName("UTF-8")
        trimTrailingWhitespace()
        endWithNewline()
        indentWithSpaces(4)
    }
}
