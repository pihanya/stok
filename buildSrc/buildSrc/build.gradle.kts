@Suppress("DSL_SCOPE_VIOLATION") // TODO: Delete after https://github.com/gradle/gradle/issues/22797
plugins {
    `kotlin-dsl`
}

buildDir = run {
    val globalBuildDir: File = projectDir.parentFile.parentFile.resolve("build")
    globalBuildDir.resolve("buildSrc").resolve("buildSrc")
}

dependencies {
    implementation(libs.gradlePlugins.kotlin)
}
