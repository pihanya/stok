package dev.stok.gradle.kotlin.extension

import org.gradle.api.artifacts.MinimalExternalModuleDependency
import org.gradle.api.artifacts.VersionCatalog
import org.gradle.api.provider.Provider

class BootstrapVersionCatalog(private val delegate: VersionCatalog) : VersionCatalog by delegate {

    val versions = BootstrapVersions(delegate)
    val kotlinx = KotlinxLibs(delegate)
    val kotlin = KotlinLibs(delegate)
    val kotest = KotestLibs(delegate)
    val kotlinLogging = delegate.findLibrary("kotlinLogging").get()

    class BootstrapVersions(private val delegate: VersionCatalog) {

        val stok = delegate.findVersion("stok").get()
        val kotlin = delegate.findVersion("kotlin").get()
        val java = delegate.findVersion("java").get()
        val gradlePlugins = GradlePluginsVersions(delegate)

        class GradlePluginsVersions(private val delegate: VersionCatalog) {

            val dokka = delegate.findVersion("gradlePlugins-dokka").get()
            val ktlint = delegate.findVersion("gradlePlugins-ktlint").get()
            val detekt = delegate.findVersion("gradlePlugins-detekt").get()
            val ktlintGradle = delegate.findVersion("gradlePlugins-ktlintGradle").get()
            val spotless = delegate.findVersion("gradlePlugins-spotless").get()
        }
    }

    class KotlinxLibs(private val delegate: VersionCatalog) {

        val coroutinesCore = delegate.findLibrary("kotlinx-coroutines-core").get()
        val datetime = delegate.findLibrary("kotlinx-datetime").get()
    }


    class KotestLibs(private val delegate: VersionCatalog) {

        val assertionsCore = delegate.findLibrary("kotest-assertionsCore").get()
        val property = delegate.findLibrary("kotest-property").get()
        val frameworkDatatest = delegate.findLibrary("kotest-frameworkDatatest").get()
        val frameworkApi = delegate.findLibrary("kotest-frameworkApi").get()
        val frameworkEngine = delegate.findLibrary("kotest-frameworkEngine").get()
        val testJunit5 = delegate.findLibrary("kotlin-test-junit5").get()
        val runnerJunit5 = delegate.findLibrary("kotest-runnerJunit5").get()
    }


    class KotlinLibs(private val delegate: VersionCatalog) {

        val test = KotlinTestLibs(delegate)

        class KotlinTestLibs(private val delegate: VersionCatalog) :
            Provider<MinimalExternalModuleDependency> by delegate.findLibrary("kotlin-test").get() {

            val junit5 = delegate.findLibrary("kotlin-test-junit5").get()
            val js = delegate.findLibrary("kotlin-test-js").get()
            val annotationsCommon = delegate.findLibrary("kotlin-test-annotationsCommon").get()
        }
    }
}
