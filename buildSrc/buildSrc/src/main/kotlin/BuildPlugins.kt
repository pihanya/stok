object BuildPlugins {

    const val SPOTLESS = "com.diffplug.spotless"
    const val DOKKA = "org.jetbrains.dokka"
    const val KOTLINX_KOVER = "org.jetbrains.kotlinx.kover"

    const val KOTLIN_MULTIPLATFORM = "org.jetbrains.kotlin.multiplatform"
    const val KOTEST_MULTIPLATFORM = "io.kotest.multiplatform"

    const val STOK_BASE = "stok.base"
    const val STOK_COMPONENT = "stok.component"
    const val STOK_LIBRARY = "stok.library"
}
