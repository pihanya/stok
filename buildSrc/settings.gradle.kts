enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

pluginManagement {
    val nexusUser = System.getProperty("stok.nexus.user")
    val nexusPassword = System.getProperty("stok.nexus.password")

    repositories {
        gradlePluginPortal()
        mavenCentral()
        google()
        maven {
            name = "stok-hosted-maven-development"
            url = uri("https://nexus.stok.dev/repository/stok-hosted-maven-development/")
            credentials {
                username = nexusUser
                password = nexusPassword
            }
        }
    }
}

dependencyResolutionManagement {
    val nexusUser = System.getProperty("stok.nexus.user")
    val nexusPassword = System.getProperty("stok.nexus.password")

    @Suppress("UnstableApiUsage")
    repositories {
        gradlePluginPortal()
        mavenCentral()
        google()
        maven {
            name = "stok-hosted-maven-development"
            url = uri("https://nexus.stok.dev/repository/stok-hosted-maven-development/")
            credentials {
                username = nexusUser
                password = nexusPassword
            }
        }
    }

    @Suppress("UnstableApiUsage")
    versionCatalogs {
        create("libs") {
            from(files("../gradle/libs.versions.toml"))
        }
    }
}
