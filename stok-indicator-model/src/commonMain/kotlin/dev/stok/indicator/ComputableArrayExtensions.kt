package dev.stok.indicator

import dev.stok.context.TimeAwareContext
import dev.stok.indicator.IndicatorValue.None
import dev.stok.indicator.IndicatorValue.Value

context(TimeAwareContext)
suspend fun <T : Any> ComputableArray<T>.getCurrent(): IndicatorValue<T> =
    getCustomDateRange(fromMoment = currentTime, toMoment = currentTime).last()

context(TimeAwareContext)
suspend fun <T : Any> ComputableArray<T>.hasCurrentValue(): Boolean =
    getCurrent().hasValue()

context(TimeAwareContext)
suspend fun <T : Any> ComputableArray<T>.getLast(): Value<T>? =
    this().asSequence().filterValues().lastOrNull()

context(TimeAwareContext)
suspend fun <T : Any> ComputableArray<T>.hasLastValue(): Boolean =
    getLast() != null

context(TimeAwareContext)
suspend fun <T : Any> ComputableArray<T>.getLastSeeking(intervalsBackoff: Int = 3): Value<T>? =
    getCustomDateRange(
        fromMoment = currentTime - (interval * intervalsBackoff),
        toMoment = currentTime,
    ).asSequence()
        .filterValues()
        .lastOrNull()

context(TimeAwareContext)
suspend fun <T : Any> ComputableArray<T>.hasLastValueSeeking(intervalsBackoff: Int = 3): Boolean =
    getLastSeeking(intervalsBackoff = intervalsBackoff) != null

context(TimeAwareContext)
suspend operator fun <T : Any> ComputableArray<T>.invoke(): List<IndicatorValue<T>> =
    getDateRange(currentTime)

fun List<IndicatorValue<*>>.countNoneValues(): Int = asSequence().countNoneValues()

fun Sequence<IndicatorValue<*>>.countNoneValues(): Int = filterIsInstance<None>().count()

fun <T : Any> List<IndicatorValue<T>>.filterValues(): List<Value<T>> = filterIsInstance<Value<T>>()

fun <T : Any> Sequence<IndicatorValue<T>>.filterValues(): Sequence<Value<T>> = filterIsInstance<Value<T>>()

fun <T : Any> List<Value<T>>.mapValues(): List<T> = map(Value<T>::value)

fun <T : Any> Sequence<Value<T>>.mapValues(): Sequence<T> = map(Value<T>::value)
