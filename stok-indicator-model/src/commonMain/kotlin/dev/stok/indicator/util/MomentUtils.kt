package dev.stok.indicator.util

import dev.stok.CandleIntervals
import dev.stok.indicator.enum.RoundingMode
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

fun computeValuesCount(interval: CandleInterval, from: Instant, to: Instant): Int =
    when {
        from > to -> 0
        from == to -> 1
        else -> ((to - from) / interval).toInt() + 1
    }

fun computeCandleMoment(moment: Instant, interval: CandleInterval, mode: RoundingMode = RoundingMode.DOWN): Instant {
    val startMoment = computeStartCandleMoment(moment, interval)
    if (startMoment == moment) return moment

    val nextMoment = startMoment + interval
    return when (mode) {
        RoundingMode.UP -> nextMoment
        RoundingMode.DOWN -> startMoment
        RoundingMode.CLEVER -> {
            val untilStart = (moment - startMoment)
            val untilEnd = (nextMoment - moment)
            if (untilStart <= untilEnd) {
                startMoment
            } else {
                nextMoment
            }
        }
    }
}

@Suppress("MagicNumber")
private fun computeStartCandleMoment(moment: Instant, interval: CandleInterval): Instant {
    check(CandleIntervals.isStandard(interval)) { "Unsupported interval: $interval" }

    val epochMinutes = moment.epochSeconds / 60
    val epochHours = epochMinutes / 60
    val epochDays = epochHours / 24
    val epochWeeks = epochDays / 7

    fun instant(epochSeconds: Long) = Instant.fromEpochSeconds(epochSeconds)
    fun Long.noRem(divider: Int): Long = minus(rem(divider))

    return when (interval) {
        CandleIntervals.INTERVAL_1MIN -> instant(60 * epochMinutes)
        CandleIntervals.INTERVAL_2MIN -> instant(60 * epochMinutes.noRem(2))
        CandleIntervals.INTERVAL_5MIN -> instant(60 * epochMinutes.noRem(5))
        CandleIntervals.INTERVAL_15MIN -> instant(60 * epochMinutes.noRem(15))
        CandleIntervals.INTERVAL_30MIN -> instant(60 * epochMinutes.noRem(30))
        CandleIntervals.INTERVAL_1HOUR -> instant(60 * 60 * epochHours)
        CandleIntervals.INTERVAL_2HOUR -> instant(60 * 60 * epochHours.noRem(2))
        CandleIntervals.INTERVAL_4HOUR -> instant(60 * 60 * epochHours.noRem(4))
        CandleIntervals.INTERVAL_1DAY -> instant(24 * 60 * 60 * epochDays)
        CandleIntervals.INTERVAL_1WEEK -> instant(7 * 24 * 60 * 60 * epochWeeks)
        else -> error("Unsupported interval: $interval")
    }
}
