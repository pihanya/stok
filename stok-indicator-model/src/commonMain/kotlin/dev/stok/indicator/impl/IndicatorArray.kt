package dev.stok.indicator.impl

import dev.stok.indicator.Indicator
import dev.stok.typing.CandleInterval
import kotlin.time.Duration

@Suppress("FunctionName")
fun <T : Any> IndicatorArray(
    indicator: Indicator<T>,
    interval: CandleInterval,
    dateRange: Duration = interval * indicator.period
): ValueSourceArray<T> = ValueSourceArray(indicator, interval, dateRange)
