package dev.stok.indicator.kotlin.extension

import dev.stok.indicator.IndicatorValueSource

fun <T : Any, R : Any> IndicatorValueSource<T>.map(block: (T) -> R): IndicatorValueSource<R> =
    IndicatorValueSource { interval, from, to ->
        this@map.getValues(interval, from, to)
            .map { indicatorValue -> indicatorValue.map(block) }
    }
