package dev.stok.indicator

interface Indicator<out T : Any> : IndicatorValueSource<T> {

    /**
     * Minimal amount of interval values (e.g. candles) required to compute indicator
     */
    val period: Int
}
