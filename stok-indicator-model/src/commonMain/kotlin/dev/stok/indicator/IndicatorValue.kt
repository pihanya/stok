package dev.stok.indicator

import dev.stok.common.util.uncheckedCast
import kotlinx.datetime.Instant

sealed interface IndicatorValue<out T : Any> {

    val moment: Instant

    val value: T?

    operator fun component1(): Instant = moment

    operator fun component2(): T? = value

    fun hasValue(): Boolean = (this is Value)

    fun <R : Any> map(block: (T) -> R): IndicatorValue<R>

    @JvmInline value class None(
        override val moment: Instant
    ) : IndicatorValue<Nothing> {

        override val value: Nothing? get() = null

        override fun <R : Any> map(block: (Nothing) -> R): IndicatorValue<R> = uncheckedCast(this)

        override fun toString(): String = "None(moment=$moment)"
    }

    data class Value<T : Any>(
        override val moment: Instant,
        override val value: T
    ) : IndicatorValue<T> {

        override fun <R : Any> map(block: (T) -> R): IndicatorValue<R> =
            Value(moment = moment, value = block(value))
    }
}
