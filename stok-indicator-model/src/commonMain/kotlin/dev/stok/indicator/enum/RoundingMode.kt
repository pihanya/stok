package dev.stok.indicator.enum

enum class RoundingMode { UP, DOWN, CLEVER }
