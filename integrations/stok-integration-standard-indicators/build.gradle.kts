plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                implementation(libs.ktor.utils)

                implementation(projects.stokDslModel)
                api(projects.stokIndicatorModel)
            }
        }
    }
}
