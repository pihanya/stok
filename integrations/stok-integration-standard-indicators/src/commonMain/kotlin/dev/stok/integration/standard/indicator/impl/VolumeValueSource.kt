package dev.stok.integration.standard.indicator.impl

import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.InstrumentCandleSource
import dev.stok.indicator.util.computeValuesCount
import dev.stok.typing.Candle
import dev.stok.typing.CandleInterval
import dev.stok.typing.Quantity
import kotlinx.datetime.Instant

class VolumeValueSource(
    private val candleSource: InstrumentCandleSource
) : IndicatorValueSource<Quantity> {

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<Quantity>> {
        val values: List<IndicatorValue<Candle>> = candleSource.getValues(interval, from = from, to = to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<Quantity>> = ArrayList(capacity)

        for (value in values) {
            result += when (value) {
                is IndicatorValue.Value -> {
                    val volume = value.value.volume
                    IndicatorValue.Value(value.moment, volume)
                }
                is IndicatorValue.None -> value // trick not to allocate new value
            }
        }

        return result
    }
}
