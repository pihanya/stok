package dev.stok.integration.standard.indicator.impl

import dev.stok.common.kotlin.extension.minus
import dev.stok.common.kotlin.extension.times
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.standard.indicator.util.takeAnyNone
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

class PercentChangeValueSource(
    private val valueSource: IndicatorValueSource<DecimalValue>,
    private val previousValueSource: IndicatorValueSource<DecimalValue>
) : IndicatorValueSource<DecimalValue> {

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<DecimalValue>> {
        val values: List<IndicatorValue<DecimalValue>> = valueSource.getValues(interval, from = from, to = to)
        val previousValues: List<IndicatorValue<DecimalValue>> = previousValueSource.getValues(interval, from, to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<DecimalValue>> = ArrayList(capacity)

        val previousAndCurrentSeq = (previousValues.asSequence() zip values.asSequence())
        for ((previous, current) in previousAndCurrentSeq) {
            result += if ((previous is IndicatorValue.Value) && (current is IndicatorValue.Value)) {
                val percentChange = 100 * ((current.value / previous.value) - 1)
                IndicatorValue.Value(current.moment, percentChange)
            } else {
                takeAnyNone(previous, current) // trick not to allocate new value
            }
        }

        return result
    }
}
