package dev.stok.integration.standard.indicator.util

import dev.stok.indicator.IndicatorValue

@Suppress("NOTHING_TO_INLINE")
internal inline fun takeAnyNone(value1: IndicatorValue<*>, value2: IndicatorValue<*>): IndicatorValue.None =
    when {
        value1 is IndicatorValue.None -> value1
        value2 is IndicatorValue.None -> value2
        else -> {
            check(value1.moment == value2.moment)
            IndicatorValue.None(value1.moment)
        }
    }
