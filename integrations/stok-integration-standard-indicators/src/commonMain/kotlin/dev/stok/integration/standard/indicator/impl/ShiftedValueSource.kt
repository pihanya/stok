package dev.stok.integration.standard.indicator.impl

import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.standard.indicator.kotlin.extension.computeAtMostValuesCount
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

/**
 * Values source whose purpose is to shift back.
 *
 * For example is [valuesShift] is 1, then for each value the previous one will be taken.
 *
 * Original: `1, 2, 3, 4, ...`
 *
 * Shift(1): `None, 1, 2, 3, ...`
 *
 * Shift(2): `None, None, 3, 4, ...`
 *
 * @property valueSource values source to be shifted
 * @property valuesShift non-negative shift
 */
class ShiftedValueSource<out T : Any>(
    private val valueSource: IndicatorValueSource<T>,
    private val valuesShift: Int
) : IndicatorValueSource<T> {

    init {
        require(valuesShift >= 0) { "Shift must not be negative" }
    }

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<T>> {
        if (valuesShift == 0) { // Fast path
            return valueSource.getValues(interval, from, to)
        }

        val values: List<IndicatorValue<T>> = valueSource.getValues(interval, from, to)
        val previousValues: MutableList<IndicatorValue.Value<T>> = valueSource.computeAtMostValuesCount(
            count = valuesShift,
            toMoment = from - interval,
            interval = interval,
        ).toMutableList()

        val intervalsCount = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<T>> = ArrayList(intervalsCount)
        repeat(intervalsCount) { index ->
            val moment = from + interval * index
            val shiftedValue = previousValues.getOrNull(previousValues.size - valuesShift)
            result += if (shiftedValue != null) {
                IndicatorValue.Value(moment, shiftedValue.value)
            } else {
                IndicatorValue.None(moment)
            }

            val actualValue = values[index]
            if (actualValue is IndicatorValue.Value<T>) {
                previousValues += actualValue
            }
        }

        return result
    }
}
