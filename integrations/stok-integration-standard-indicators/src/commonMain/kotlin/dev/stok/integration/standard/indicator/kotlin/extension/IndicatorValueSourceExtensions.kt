package dev.stok.integration.standard.indicator.kotlin.extension

import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValue.Value
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.filterValues
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

internal suspend fun <T : Any> IndicatorValueSource<T>.computeAtMostValuesCount(
    count: Int,
    toMoment: Instant,
    interval: CandleInterval
): List<Value<T>> = doComputeAtMostValues(
    valueSource = this,
    currentValues = emptyList(),
    count = count,
    toMoment = toMoment,
    interval = interval,
)

private tailrec suspend fun <T : Any> doComputeAtMostValues(
    valueSource: IndicatorValueSource<T>,
    currentValues: List<IndicatorValue<T>>,
    count: Int,
    toMoment: Instant,
    interval: CandleInterval
): List<Value<T>> {
    val validValuesCount = currentValues.count(IndicatorValue<T>::hasValue)
    if (validValuesCount >= count) {
        return currentValues.filterValues().takeLast(count)
    }

    // TODO: replace heuristic with maximal backoff period
    run { // heuristic
        val sequentialNones = currentValues.indexOfFirst(IndicatorValue<T>::hasValue)
            .takeIf { it != -1 } ?: currentValues.size
        if (sequentialNones >= 18 * count) {
            return currentValues.filterValues()
        }
    }

    lateinit var currentFrom: Instant
    val additionalValues = when {
        currentValues.isEmpty() -> {
            currentFrom = toMoment - interval * (count - 1)
            valueSource.getValues(interval, from = currentFrom, to = toMoment)
        }

        else -> {
            val currentTo = currentValues.first().moment - interval
            val minimalBackoffPeriod = interval * maxOf(count - validValuesCount, 1)
            val backoffMultiplier = 8 // heuristic

            currentFrom = currentTo - minimalBackoffPeriod * backoffMultiplier
            valueSource.getValues(interval, from = currentFrom, to = currentTo)
        }
    }

    return doComputeAtMostValues(
        valueSource = valueSource,
        currentValues = additionalValues + currentValues,
        interval = interval,
        count = count,
        toMoment = currentFrom,
    )
}
