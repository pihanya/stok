package dev.stok.integration.standard.indicator

internal object JvmParameters {

    /**
     * Whether candles loaded from external QuoteApi are cached in memory
     */
    val candlesCacheEnabled: Boolean = System.getProperty("stok.standardIndicators.candlesCacheEnabled", "true").toBoolean()
}
