package dev.stok.integration.standard.indicator.impl

import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.InstrumentCandleSource
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.standard.indicator.enum.CandlePriceType
import dev.stok.typing.Candle
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

class PriceValueSource(
    private val candleSource: InstrumentCandleSource,
    val priceType: CandlePriceType
) : IndicatorValueSource<DecimalValue> {

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<DecimalValue>> {
        val values: List<IndicatorValue<Candle>> = candleSource.getValues(interval, from = from, to = to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<DecimalValue>> = ArrayList(capacity)

        for (value in values) {
            result += when (value) {
                is IndicatorValue.Value -> {
                    val priceValue = when (priceType) {
                        CandlePriceType.OPEN -> value.value.open
                        CandlePriceType.CLOSE -> value.value.close
                        CandlePriceType.HIGH -> value.value.high
                        CandlePriceType.LOW -> value.value.low
                    }
                    IndicatorValue.Value(value.moment, priceValue)
                }
                is IndicatorValue.None -> value // trick not to allocate new value
            }
        }

        return result
    }
}
