package dev.stok.integration.standard.indicator.model

import dev.stok.common.typing.DecimalValue

class MacdIndicatorValue(
    val macd: DecimalValue,
    val signal: DecimalValue,
    val histogram: DecimalValue
) {

    fun component1(): DecimalValue = macd

    fun component2(): DecimalValue = signal

    fun component3(): DecimalValue = histogram

    override fun toString(): String =
        "MacdIndicatorValue(macd=${macd.toDouble()}, signal=${signal.toDouble()}, histogram=${histogram.toDouble()})"
}
