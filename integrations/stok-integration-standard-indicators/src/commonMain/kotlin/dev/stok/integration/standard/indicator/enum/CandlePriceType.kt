package dev.stok.integration.standard.indicator.enum

enum class CandlePriceType { OPEN, CLOSE, HIGH, LOW }
