package dev.stok.integration.standard.indicator.impl

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.Indicator
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.standard.indicator.util.takeAnyNone
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

@InternalLibraryApi
class MacdLineIndicator(
    valueSource: IndicatorValueSource<DecimalValue>,
    private val fastEmaPeriod: Int,
    private val slowEmaPeriod: Int
) : Indicator<DecimalValue> {

    override val period: Int

    private val fastEmaIndicator: Indicator<DecimalValue> = EmaIndicator(valueSource, period = fastEmaPeriod)

    private val slowEmaIndicator: Indicator<DecimalValue> = EmaIndicator(valueSource, period = slowEmaPeriod)

    init {
        require(slowEmaPeriod > fastEmaPeriod) {
            "Slow EMA period [$slowEmaPeriod] must be greater than Fast EMA period [$fastEmaPeriod]"
        }
        this.period = maxOf(slowEmaIndicator.period, fastEmaIndicator.period)
    }

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<DecimalValue>> {
        val fastEmaValues: List<IndicatorValue<DecimalValue>> = fastEmaIndicator.getValues(interval, from, to)
        val slowEmaValues: List<IndicatorValue<DecimalValue>> = slowEmaIndicator.getValues(interval, from, to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<DecimalValue>> = ArrayList(capacity)

        val fastEmaWithSlowEmaSeq = fastEmaValues.asSequence() zip slowEmaValues.asSequence()
        for ((fastEma, slowEma) in fastEmaWithSlowEmaSeq) {
            result += if ((fastEma is IndicatorValue.Value) && (slowEma is IndicatorValue.Value)) {
                val macdLineValue = fastEma.value - slowEma.value
                IndicatorValue.Value(fastEma.moment, macdLineValue)
            } else {
                takeAnyNone(fastEma, slowEma) // trick not to allocate new value
            }
        }

        return result
    }
}
