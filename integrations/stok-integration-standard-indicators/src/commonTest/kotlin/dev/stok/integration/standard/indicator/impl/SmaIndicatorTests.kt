package dev.stok.integration.standard.indicator.impl

import dev.stok.CandleIntervals
import dev.stok.common.kotlin.extension.absolute
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.IndicatorValue.Value
import dev.stok.integration.standard.indicator.util.AdHocValueSource
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.comparables.shouldBeLessThan
import io.kotest.matchers.types.shouldBeInstanceOf
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant

class SmaIndicatorTests : ShouldSpec({

    val from = LocalDateTime.parse("2007-12-30T00:00:00").toInstant(TimeZone.UTC)
    val interval = CandleIntervals.INTERVAL_5MIN
    val source = AdHocValueSource<DecimalValue>()

    suspend fun SmaIndicator.doTestSingleValue(
        expected: DecimalValue,
        moment: Instant = from,
        precision: DecimalValue = "0.01".toDecimal()
    ) {
        val indicator = this
        val values = indicator.getValues(interval, moment, moment)

        val result = values
            .shouldHaveSize(1)
            .first()
            .shouldBeInstanceOf<Value<DecimalValue>>()
            .value

        withClue("$result - $expected < $precision") {
            (result - expected).absolute() shouldBeLessThan precision
        }
    }

    context("sma(9) positive tests") {

        val period = 9
        val indicator = SmaIndicator(source, period = period)

        should("const value is equals same const") {
            source.values = (1..period)
                .map { ord -> Value(from - interval * (period - ord), 1.toDecimal()) }
                .toTypedArray()

            indicator.doTestSingleValue(expected = 1.toDecimal())
        }

        should("growing sequence is equals mean") {
            source.values = (1..period) // 1, 2, 3, ..., 9
                .map { ord -> Value(from - interval * (period - ord), ord.toDecimal()) }
                .toTypedArray()

            indicator.doTestSingleValue(expected = 5.toDecimal())
        }

        should("decreasing sequence is equals mean") {
            source.values = (1..period) // 9, 8, 7, ..., 1
                .map { ord -> Value(from - interval * (period - ord), (period - ord + 1).toDecimal()) }
                .toTypedArray()

            indicator.doTestSingleValue(expected = 5.toDecimal())
        }
    }

    context("sma(12) positive tests") {

        val period = 12
        val indicator = SmaIndicator(source, period = period)

        should("const value is equals same const") {
            source.values = (1..period)
                .map { ord -> Value(from - interval * (period - ord), 1.toDecimal()) }
                .toTypedArray()

            indicator.doTestSingleValue(expected = 1.toDecimal())
        }

        should("growing sequence is equals mean") {
            source.values = (1..period) // 1, 2, 3, ..., 12
                .map { ord -> Value(from - interval * (period - ord), ord.toDecimal()) }
                .toTypedArray()

            indicator.doTestSingleValue(expected = "6.5".toDecimal())
        }

        should("decreasing sequence is equals mean") {
            source.values = (1..period) // 12, 11, 10, ..., 1
                .map { ord -> Value(from - interval * (period - ord), (period - ord + 1).toDecimal()) }
                .toTypedArray()

            indicator.doTestSingleValue(expected = "6.5".toDecimal())
        }
    }
})
