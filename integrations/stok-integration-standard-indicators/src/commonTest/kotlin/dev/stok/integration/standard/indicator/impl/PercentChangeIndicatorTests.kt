package dev.stok.integration.standard.indicator.impl

import dev.stok.CandleIntervals
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.IndicatorValue.Value
import dev.stok.integration.standard.indicator.util.AdHocValueSource
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant

class PercentChangeIndicatorTests : ShouldSpec({

    val from = LocalDateTime.parse("2007-12-30T00:00:00").toInstant(TimeZone.UTC)
    val interval = CandleIntervals.INTERVAL_5MIN
    val source = AdHocValueSource<DecimalValue>()

    should("single change succeeds") {
        val indicator = PercentChangeValueSource(
            valueSource = source,
            previousValueSource = ShiftedValueSource(source, valuesShift = 1),
        )

        source.values = arrayOf(
            Value(from - interval, "10".toDecimal()),
            Value(from, "20".toDecimal()),
        )

        val value = indicator.getValues(interval, from, from).first()
        value.shouldBeInstanceOf<Value<DecimalValue>>()
        value.value shouldBe "100".toDecimal()
    }

    should("multiple changes succeeds") {
        val indicator = PercentChangeValueSource(
            valueSource = source,
            previousValueSource = ShiftedValueSource(source, valuesShift = 1),
        )

        source.values = arrayOf(
            Value(from + (interval * (-1)), "10".toDecimal()),
            Value(from + (interval * 0), "20".toDecimal()),
            Value(from + (interval * 1), "30".toDecimal()),
            Value(from + (interval * 2), "40".toDecimal()),
        )

        val values = indicator.getValues(interval, from, from + (interval * 2))
        values.shouldHaveSize(3)

        val firstValue = values[0].shouldBeInstanceOf<Value<DecimalValue>>()
        firstValue.value shouldBe "100".toDecimal() // 10 -> 20

        val secondValue = values[1].shouldBeInstanceOf<Value<DecimalValue>>()
        secondValue.value shouldBe "50".toDecimal() // 20 -> 30

        val thirdValue = values[2].shouldBeInstanceOf<Value<DecimalValue>>()
        thirdValue.value shouldBe "33.33333333333333333333333333333330".toDecimal() // 30 -> 40
    }
})
