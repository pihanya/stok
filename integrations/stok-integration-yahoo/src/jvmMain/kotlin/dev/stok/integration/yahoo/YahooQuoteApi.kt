package dev.stok.integration.yahoo

import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.enum.InstrumentIdType
import dev.stok.integration.api.QuoteApi
import dev.stok.integration.model.CandleRequest
import dev.stok.integration.model.OrderBook
import dev.stok.integration.stereotype.CoroutineScopeAware
import dev.stok.typing.CandleInterval
import dev.stok.typing.HistoricCandle
import dev.stok.typing.InstrumentId
import kotlinx.coroutines.CoroutineScope
import kotlinx.datetime.Instant
import yahoofinance.YahooFinance
import yahoofinance.quotes.stock.StockQuote

class YahooQuoteApi : QuoteApi, CoroutineScopeAware {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> = setOf(InstrumentIdType.Ticker)

    private lateinit var scope: CoroutineScope

    val subscribedStocks = mutableSetOf<InstrumentId>()

    val rememberedQuotes = mutableListOf<StockObservation>()

    data class StockObservation(val quote: StockQuote, val moment: Instant)

    override suspend fun getCandles(request: CandleRequest): List<HistoricCandle> {
        val stock = YahooFinance.get(request.instrumentId.value)
        val quote = stock.getQuote(true)

        return buildList {
            var moment = request.from
            while (moment + request.interval <= request.to) {
                val candle = candleFor(request.instrumentId, request.interval, moment)
                if (candle != null) add(candle)
                moment += request.interval
            }
        }
    }

    override suspend fun getOrderBook(instrumentId: InstrumentId, depth: Short): OrderBook {
        TODO("Not yet implemented")
    }

    private fun candleFor(instrumentId: InstrumentId, interval: CandleInterval, from: Instant): HistoricCandle? {
        val to = from + interval

        val candleQuotes = rememberedQuotes
            .filter { it.moment in (from..to) }
            .filter { it.quote.symbol == instrumentId.value }
            .sortedBy { it.moment }
            .map { it.quote }

        if (candleQuotes.isEmpty()) return null

        return HistoricCandle(
            open = candleQuotes.first().open.toDecimal(),
            close = candleQuotes.last().price.toDecimal(),
            low = candleQuotes.minOf { it.price }.toDecimal(),
            high = candleQuotes.maxOf { it.price }.toDecimal(),
            volume = candleQuotes.maxOf { it.volume },
            interval = interval,
            openTime = from,
        )
    }

    override fun setCoroutineScope(scope: CoroutineScope) {
        this.scope = scope
    }
}
