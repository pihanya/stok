package dev.stok.integration

import dev.stok.integration.context.BootstrapBuilderContext
import dev.stok.integration.yahoo.YahooQuoteApi

fun BootstrapBuilderContext.yahoo() {
    val context: BootstrapBuilderContext = this@yahoo

    context.setByName(BootstrapStandardNames.quoteApi, YahooQuoteApi())
}
