plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.stokIntegrationModel)
            }
        }
        jvmMain {
            dependencies {
                implementation("io.finnhub:kotlin-client:2.0.19")
            }
        }
    }
}
