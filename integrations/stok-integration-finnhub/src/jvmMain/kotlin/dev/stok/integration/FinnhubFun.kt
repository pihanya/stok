package dev.stok.integration

import dev.stok.integration.context.BootstrapBuilderContext
import dev.stok.integration.finnhub.api.FinnhubQuoteApi
import dev.stok.integration.stereotype.BootstrapDslMarker

fun BootstrapBuilderContext.finnhub(closure: FinnhubContextBuilder.() -> Unit) {
    val context: BootstrapBuilderContext = this@finnhub

    val builder = FinnhubContextBuilder()
    closure(builder)

    val finnhubContext = builder.build()

    context.setByName(
        BootstrapStandardNames.quoteApi,
        FinnhubQuoteApi(apiKey = finnhubContext.apiKey),
    )
}

@BootstrapDslMarker
class FinnhubContextBuilder {

    private lateinit var apiKey: String

    fun apiKey(value: String) = apply { this.apiKey = value }

    fun build(): FinnhubContext =
        FinnhubContext(
            apiKey = apiKey,
        )
}

data class FinnhubContext(
    val apiKey: String
)
