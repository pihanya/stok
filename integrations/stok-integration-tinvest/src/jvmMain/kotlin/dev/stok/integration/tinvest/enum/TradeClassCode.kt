package dev.stok.integration.tinvest.enum

/**
 * [GlobalExchanges. RUSSIA: MOEX Starts Trading in SBP Exchange Shares](https://globalexchanges.com/latest-news/russia-moex-starts-trading-in-sbp-exchange-shares/127704/)
 *
 * [MOEX. Preliminary parameters for the start of trading in SPB Exchange ordinary shares](https://www.moex.com/n37801)
 */
internal enum class TradeClassCode(val code: String) {

    CENTRAL_ORDER_BOOK("TQBR"),

    NEGOTIATED_TRADES("SPEQ"),

    NEGOTIATED_TRADES_WITH_CCP("PTEQ");

    companion object {

        fun fromCode(value: String): TradeClassCode =
            TradeClassCode.values().single { it.code == value }
    }
}
