package dev.stok.integration.tinvest.api

import com.benasher44.uuid.Uuid
import dev.stok.common.kotlinx.coroutines.ComputingRateLimiter
import dev.stok.common.kotlinx.coroutines.JoinedRateLimiter
import dev.stok.common.kotlinx.coroutines.RateLimiter
import dev.stok.common.typing.DecimalValue
import dev.stok.enum.InstrumentIdType
import dev.stok.enum.TradeDirection
import dev.stok.integration.api.OrderApi
import dev.stok.integration.stereotype.CoroutineScopeAware
import dev.stok.integration.tinvest.JvmParameters
import dev.stok.integration.tinvest.SUPPORTED_INSTRUMENT_ID_TYPES
import dev.stok.integration.tinvest.util.computeRateLimitRefillTime
import dev.stok.integration.tinvest.util.rpsServiceLimiter
import dev.stok.integration.tinvest.util.toTinvestQuotation
import dev.stok.model.LimitOrder
import dev.stok.model.MarketOrder
import dev.stok.model.OrderExecutionInfo
import dev.stok.model.OrderInfo
import dev.stok.model.StokAccount
import dev.stok.model.StokOrder
import dev.stok.stereotype.StatefulElement
import dev.stok.typing.InstrumentId
import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.future.await
import kotlinx.datetime.Clock
import ru.tinkoff.piapi.contract.v1.OrderDirection
import ru.tinkoff.piapi.contract.v1.OrderType
import ru.tinkoff.piapi.core.InvestApi
import ru.tinkoff.piapi.core.OrdersService

class TinvestOrderApi(
    private val token: String
) : OrderApi, CoroutineScopeAware, StatefulElement<TinvestOrderApi> {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> by ::SUPPORTED_INSTRUMENT_ID_TYPES

    private val orderService: OrdersService = InvestApi.create(token).ordersService

    private lateinit var orderServiceRateLimiter: RateLimiter

    override fun setCoroutineScope(scope: CoroutineScope) {
        this.orderServiceRateLimiter = with(scope) {
            JoinedRateLimiter(
                ComputingRateLimiter(
                    limit = JvmParameters.ordersServiceRpmLimit,
                    refillMarkFn = { computeRateLimitRefillTime(Clock.System) },
                ),
                rpsServiceLimiter(),
            )
        }
    }

    override suspend fun marketBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder {
        val orderId = Uuid.randomUUID().toString()

        val response = orderService.postOrder(
            /* instrumentId = */ instrumentId.value,
            /* quantity = */ quantity,
            /* price = */ DecimalValue.ZERO.toTinvestQuotation(),
            /* direction = */ OrderDirection.ORDER_DIRECTION_BUY,
            /* accountId = */ account.id,
            /* type = */ OrderType.ORDER_TYPE_MARKET,
            /* orderId = */ orderId,
        ).await()

        return MarketOrder(
            direction = TradeDirection.BUY,
            instrumentId = instrumentId,
            lots = quantity,
            orderInfo = OrderInfo(
                id = orderId,
                accountId = account.id,
                creationDate = TODO(),
            ),
            executionInfo = OrderExecutionInfo(
                lotsExecuted = response.lotsExecuted,
                executedCommission = TODO(),
                executionDate = TODO(),
            ),
        )

        TODO("Not yet implemented")
    }

    override suspend fun marketSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder {
        TODO("Not yet implemented")
    }

    override suspend fun limitBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder {
        TODO("Not yet implemented")
    }

    override suspend fun limitSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder {
        TODO("Not yet implemented")
    }

    override suspend fun getActiveOrders(account: StokAccount): List<StokOrder> {
        TODO("Not yet implemented")
    }

    override suspend fun cancelOrder(order: StokOrder) {
        TODO("Not yet implemented")
    }

    override fun clone(): TinvestOrderApi =
        TinvestOrderApi(token)
}
