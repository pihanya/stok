package dev.stok.integration.tinvest.api

import dev.stok.common.kotlinx.coroutines.ComputingRateLimiter
import dev.stok.common.kotlinx.coroutines.JoinedRateLimiter
import dev.stok.common.kotlinx.coroutines.RateLimiter
import dev.stok.integration.api.PortfolioApi
import dev.stok.integration.stereotype.CoroutineScopeAware
import dev.stok.integration.tinvest.JvmParameters
import dev.stok.integration.tinvest.util.computeRateLimitRefillTime
import dev.stok.integration.tinvest.util.rpsServiceLimiter
import dev.stok.model.StokAccount
import dev.stok.model.StokPosition
import dev.stok.stereotype.StatefulElement
import dev.stok.typing.CurrencyCode
import dev.stok.typing.MoneyValue
import kotlinx.coroutines.CoroutineScope
import kotlinx.datetime.Clock
import ru.tinkoff.piapi.core.InvestApi
import ru.tinkoff.piapi.core.OperationsService

class TinvestPortfolioApi(
    private val token: String
) : PortfolioApi, CoroutineScopeAware, StatefulElement<TinvestPortfolioApi> {

    private val operationsService: OperationsService = InvestApi.create(token).operationsService

    private lateinit var operationsServiceRateLimiter: RateLimiter

    override fun setCoroutineScope(scope: CoroutineScope) {
        this.operationsServiceRateLimiter = with(scope) {
            JoinedRateLimiter(
                ComputingRateLimiter(
                    limit = JvmParameters.operationsServiceRpmLimit,
                    refillMarkFn = { computeRateLimitRefillTime(Clock.System) },
                ),
                rpsServiceLimiter(),
            )
        }
    }

    override suspend fun getAllMoney(account: StokAccount): List<MoneyValue> {
        TODO("Not yet implemented")
    }

    override suspend fun getMoney(currency: CurrencyCode, account: StokAccount): MoneyValue {
        TODO("Not yet implemented")
    }

    override suspend fun getPositions(account: StokAccount): List<StokPosition> {
        TODO("Not yet implemented")
    }

    override fun clone(): TinvestPortfolioApi =
        TinvestPortfolioApi(token = token)
}
