package dev.stok.integration.tinvest.api

import dev.stok.CandleIntervals.INTERVAL_10MIN
import dev.stok.CandleIntervals.INTERVAL_15MIN
import dev.stok.CandleIntervals.INTERVAL_1DAY
import dev.stok.CandleIntervals.INTERVAL_1HOUR
import dev.stok.CandleIntervals.INTERVAL_1MIN
import dev.stok.CandleIntervals.INTERVAL_1WEEK
import dev.stok.CandleIntervals.INTERVAL_2HOUR
import dev.stok.CandleIntervals.INTERVAL_2MIN
import dev.stok.CandleIntervals.INTERVAL_30MIN
import dev.stok.CandleIntervals.INTERVAL_3MIN
import dev.stok.CandleIntervals.INTERVAL_4HOUR
import dev.stok.CandleIntervals.INTERVAL_5MIN
import dev.stok.common.kotlin.extension.mapAsync
import dev.stok.common.kotlinx.coroutines.ComputingRateLimiter
import dev.stok.common.kotlinx.coroutines.JoinedRateLimiter
import dev.stok.common.kotlinx.coroutines.RateLimiter
import dev.stok.common.util.uncheckedCast
import dev.stok.enum.InstrumentIdType
import dev.stok.integration.api.InstrumentApi
import dev.stok.integration.api.QuoteApi
import dev.stok.integration.model.CandleRequest
import dev.stok.integration.model.OrderBook
import dev.stok.integration.model.OrderBookOrder
import dev.stok.integration.stereotype.CoroutineScopeAware
import dev.stok.integration.tinvest.JvmParameters
import dev.stok.integration.tinvest.SUPPORTED_INSTRUMENT_ID_TYPES
import dev.stok.integration.tinvest.util.computeRateLimitRefillTime
import dev.stok.integration.tinvest.util.rpsServiceLimiter
import dev.stok.integration.tinvest.util.toDecimal
import dev.stok.integration.tinvest.util.toInstant
import dev.stok.stereotype.StatefulElement
import dev.stok.typing.CandleInterval
import dev.stok.typing.Figi
import dev.stok.typing.HistoricCandle
import dev.stok.typing.InstrumentId
import dev.stok.typing.TimeAwareCandle
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.future.await
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.toJavaInstant
import ru.tinkoff.piapi.contract.v1.Order
import ru.tinkoff.piapi.core.InvestApi
import ru.tinkoff.piapi.core.MarketDataService
import kotlin.coroutines.cancellation.CancellationException
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days

private typealias TinvestCandleInterval = ru.tinkoff.piapi.contract.v1.CandleInterval
private typealias TinvestCandle = ru.tinkoff.piapi.contract.v1.HistoricCandle
private typealias TinvestOrderBook = ru.tinkoff.piapi.contract.v1.GetOrderBookResponse

class TinvestQuoteApi(
    private val token: String
) : QuoteApi, CoroutineScopeAware, StatefulElement<TinvestQuoteApi> {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> by ::SUPPORTED_INSTRUMENT_ID_TYPES

    private val marketDataService: MarketDataService = InvestApi.create(token).marketDataService

    private lateinit var instrumentApi: InstrumentApi

    private lateinit var marketDataServiceRateLimiter: RateLimiter

    override fun setCoroutineScope(scope: CoroutineScope) {
        this.instrumentApi = TinvestInstrumentApi(token)
            .apply { setCoroutineScope(scope) }
        this.marketDataServiceRateLimiter = with(scope) {
            JoinedRateLimiter(
                ComputingRateLimiter(
                    limit = JvmParameters.marketDataServiceRpmLimit,
                    refillMarkFn = { computeRateLimitRefillTime(Clock.System) },
                ),
                rpsServiceLimiter(),
            )
        }
    }

    override suspend fun getCandles(request: CandleRequest): List<TimeAwareCandle> {
        val figi: Figi = instrumentApi.convertInstrumentId(request.instrumentId, InstrumentIdType.Figi)
            .let(::uncheckedCast)

        val apiRequests = buildApiRequests(
            from = request.from,
            to = request.to,
            figi = figi,
            interval = request.interval,
        )

        return try {
            apiRequests
                .mapAsync { request -> getDomainCandles(request) }
                .asSequence()
                .flatMap { candles ->
                    candles.asSequence()
                        .map { mapToModelCandle(it, request.interval) }
                }
                .toList()
        } catch (ex: Throwable) {
            if (ex is CancellationException) throw ex
            throw LOGGER.throwing(RuntimeException("Could not get candles for [$figi]: ${ex.message}", ex))
        }
    }

    override suspend fun getOrderBook(instrumentId: InstrumentId, depth: Short): OrderBook {
        require(depth in SupportedOrderBookDepths.values) {
            "Order book depth [$depth] is not supported." +
                " List of supported depths: [${SupportedOrderBookDepths.values.joinToString()}]"
        }

        val figi = instrumentApi.convertInstrumentId(instrumentId, InstrumentIdType.Figi)
        val domainOrderBook = getDomainOrderBook(figi, depth)
        return mapToModelOrderBook(domainOrderBook)
    }

    private suspend fun getDomainOrderBook(figi: InstrumentId, depth: Short): TinvestOrderBook {
        marketDataServiceRateLimiter.takePermit()
        val future = marketDataService.getOrderBook(
            /* instrumentId = */ figi.value,
            /* depth = */ depth.toInt(),
        )

        val response: TinvestOrderBook = future.await()
        LOGGER.trace { "Got order book [$response] candles for instrument id [$figi] (depth [$depth])" }
        return response
    }

    private suspend fun getDomainCandles(request: CandleRequest): List<TinvestCandle> {
        marketDataServiceRateLimiter.takePermit()
        val future = marketDataService.getCandles(
            /* instrumentId = */ request.instrumentId.value,
            /* from = */ request.from.toJavaInstant(),
            /* to = */ request.to.toJavaInstant(),
            /* interval = */ toDomainCandleInterval(request.interval),
        )

        val response: List<TinvestCandle> = future.await()
        LOGGER.trace { "Got [${response.size}] candles for request [$request]" }
        return response
    }

    private fun buildApiRequests(
        from: Instant,
        to: Instant,
        figi: Figi,
        interval: CandleInterval
    ): List<CandleRequest> {
        val maxLoadPeriod = getMaxLoadPeriod(interval)

        val capacity = 1 + ((to - from) / maxLoadPeriod).toInt()
        val result: MutableList<CandleRequest> = ArrayList(capacity)

        var currentFrom = from
        while (currentFrom < to) {
            val currentTo = minOf(to, currentFrom + maxLoadPeriod)
            result += CandleRequest(
                instrumentId = figi, interval = interval,
                from = currentFrom, to = currentTo,
            )
            currentFrom = currentTo
        }

        return result
    }

    private fun mapToModelOrderBook(response: TinvestOrderBook): OrderBook {
        fun mapToModelOrder(order: Order) = OrderBookOrder(price = order.price.toDecimal(), quantity = order.quantity)

        return OrderBook(
            asks = response.asksList.map(::mapToModelOrder).sortedBy(OrderBookOrder::price),
            bids = response.bidsList.map(::mapToModelOrder).sortedBy(OrderBookOrder::price),
        )
    }

    private fun mapToModelCandle(candle: TinvestCandle, interval: CandleInterval): HistoricCandle =
        HistoricCandle(
            open = candle.open.toDecimal(),
            close = candle.close.toDecimal(),
            low = candle.low.toDecimal(),
            high = candle.high.toDecimal(),
            volume = candle.volume,
            interval = interval,
            openTime = candle.time.toInstant(),
        )

    /**
     * [Tinkoff Invest API. Load History](https://tinkoff.github.io/investAPI/load_history/)
     */
    private fun getMaxLoadPeriod(interval: CandleInterval): Duration =
        when (interval) {
            INTERVAL_1MIN, INTERVAL_2MIN, INTERVAL_3MIN,
            INTERVAL_5MIN, INTERVAL_10MIN, INTERVAL_15MIN -> 1.days

            INTERVAL_30MIN -> 2.days
            INTERVAL_1HOUR -> 7.days
            INTERVAL_2HOUR, INTERVAL_4HOUR -> 30.days
            INTERVAL_1DAY -> 360.days
            INTERVAL_1WEEK -> 360.days * 2
            else -> 1.days
        }

    private fun toDomainCandleInterval(value: CandleInterval): TinvestCandleInterval = when (value) {
        INTERVAL_1MIN -> TinvestCandleInterval.CANDLE_INTERVAL_1_MIN
        INTERVAL_2MIN -> TinvestCandleInterval.CANDLE_INTERVAL_2_MIN
        INTERVAL_3MIN -> TinvestCandleInterval.CANDLE_INTERVAL_3_MIN
        INTERVAL_5MIN -> TinvestCandleInterval.CANDLE_INTERVAL_5_MIN
        INTERVAL_10MIN -> TinvestCandleInterval.CANDLE_INTERVAL_10_MIN
        INTERVAL_15MIN -> TinvestCandleInterval.CANDLE_INTERVAL_15_MIN
        INTERVAL_30MIN -> TinvestCandleInterval.CANDLE_INTERVAL_30_MIN
        INTERVAL_1HOUR -> TinvestCandleInterval.CANDLE_INTERVAL_HOUR
        INTERVAL_2HOUR -> TinvestCandleInterval.CANDLE_INTERVAL_2_HOUR
        INTERVAL_4HOUR -> TinvestCandleInterval.CANDLE_INTERVAL_4_HOUR
        INTERVAL_1DAY -> TinvestCandleInterval.CANDLE_INTERVAL_DAY
        INTERVAL_1WEEK -> TinvestCandleInterval.CANDLE_INTERVAL_WEEK
        else -> error("Unsupported interval: $value")
    }

    override fun clone(): TinvestQuoteApi =
        TinvestQuoteApi(
            token = token,
        )

    companion object {

        private val LOGGER: KLogger = KotlinLogging.logger {}
    }

    /**
     * [Tinkoff Invest API. Market Data Service](https://tinkoff.github.io/investAPI/marketdata/#subscriptionstatus)
     */
    object SupportedOrderBookDepths {

        const val DEPTH_1: Short = 1
        const val DEPTH_10: Short = 10
        const val DEPTH_20: Short = 20
        const val DEPTH_30: Short = 30
        const val DEPTH_40: Short = 40
        const val DEPTH_50: Short = 50

        @Suppress("ktlint:standard:wrap-argument-list")
        val values: List<Short> = listOf(
            DEPTH_1, DEPTH_10,
            DEPTH_20, DEPTH_30,
            DEPTH_40, DEPTH_50,
        )
    }
}
