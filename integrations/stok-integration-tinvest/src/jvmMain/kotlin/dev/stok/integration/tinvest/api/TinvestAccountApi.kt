package dev.stok.integration.tinvest.api

import dev.stok.common.kotlinx.coroutines.ComputingRateLimiter
import dev.stok.common.kotlinx.coroutines.JoinedRateLimiter
import dev.stok.common.kotlinx.coroutines.RateLimiter
import dev.stok.integration.api.AccountApi
import dev.stok.integration.enum.AccountAccessLevel
import dev.stok.integration.enum.AccountStatus
import dev.stok.integration.model.StokAccountDetails
import dev.stok.integration.model.StokAccountSummary
import dev.stok.integration.stereotype.CoroutineScopeAware
import dev.stok.integration.tinvest.JvmParameters
import dev.stok.integration.tinvest.util.computeRateLimitRefillTime
import dev.stok.integration.tinvest.util.rpsServiceLimiter
import dev.stok.stereotype.StatefulElement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.future.await
import kotlinx.datetime.Clock
import ru.tinkoff.piapi.core.InvestApi
import ru.tinkoff.piapi.core.UsersService

private typealias TinvestAccount = ru.tinkoff.piapi.contract.v1.Account
private typealias TinvestAccountStatus = ru.tinkoff.piapi.contract.v1.AccountStatus
private typealias TinvestAccessLevel = ru.tinkoff.piapi.contract.v1.AccessLevel

class TinvestAccountApi(
    private val token: String
) : AccountApi, CoroutineScopeAware, StatefulElement<TinvestAccountApi> {

    private val userService: UsersService = InvestApi.create(token).userService

    private lateinit var userServiceRateLimiter: RateLimiter

    override fun setCoroutineScope(scope: CoroutineScope) {
        this.userServiceRateLimiter = with(scope) {
            JoinedRateLimiter(
                ComputingRateLimiter(
                    limit = JvmParameters.usersServiceRpmLimit,
                    refillMarkFn = { computeRateLimitRefillTime(Clock.System) },
                ),
                rpsServiceLimiter(),
            )
        }
    }

    override suspend fun getAccounts(): List<StokAccountSummary> {
        userServiceRateLimiter.takePermit()
        val accounts = userService.accounts.await()

        return accounts.map { account ->
            StokAccountSummary(
                id = account.id,
                name = account.name,
            )
        }
    }

    override suspend fun getAccountById(id: String): StokAccountDetails {
        userServiceRateLimiter.takePermit()
        val accounts = userService.accounts.await()
        return accounts.map(::mapToDetails).single { it.id == id }
    }

    private fun mapToDetails(account: TinvestAccount): StokAccountDetails =
        StokAccountDetails(
            id = account.id,
            name = account.name,
            status = when (account.status) {
                TinvestAccountStatus.ACCOUNT_STATUS_NEW -> AccountStatus.CREATED
                TinvestAccountStatus.ACCOUNT_STATUS_OPEN -> AccountStatus.ACTIVE
                TinvestAccountStatus.ACCOUNT_STATUS_CLOSED -> AccountStatus.CLOSED

                TinvestAccountStatus.ACCOUNT_STATUS_UNSPECIFIED,
                TinvestAccountStatus.UNRECOGNIZED -> AccountStatus.UNKNOWN
            },
            accessLevel = when (account.accessLevel) {
                TinvestAccessLevel.ACCOUNT_ACCESS_LEVEL_FULL_ACCESS -> AccountAccessLevel.FULL_ACCESS
                TinvestAccessLevel.ACCOUNT_ACCESS_LEVEL_READ_ONLY -> AccountAccessLevel.READONLY
                TinvestAccessLevel.ACCOUNT_ACCESS_LEVEL_NO_ACCESS -> AccountAccessLevel.NO_ACCESS

                TinvestAccessLevel.ACCOUNT_ACCESS_LEVEL_UNSPECIFIED,
                TinvestAccessLevel.UNRECOGNIZED -> AccountAccessLevel.UNKNOWN
            },
        )

    override fun clone(): TinvestAccountApi =
        TinvestAccountApi(token = token)
}
