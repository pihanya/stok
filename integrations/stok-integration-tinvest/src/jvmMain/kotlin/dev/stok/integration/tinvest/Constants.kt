package dev.stok.integration.tinvest

import dev.stok.enum.InstrumentIdType

val UID_CUSTOM_INSTRUMENT_TYPE = InstrumentIdType.Custom("Uid")

val SUPPORTED_INSTRUMENT_ID_TYPES: Set<InstrumentIdType> = setOf(
    InstrumentIdType.Figi,
    InstrumentIdType.Ticker,
    InstrumentIdType.Isin,
    UID_CUSTOM_INSTRUMENT_TYPE,
)
