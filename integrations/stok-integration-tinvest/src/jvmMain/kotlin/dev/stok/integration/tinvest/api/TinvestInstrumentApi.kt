package dev.stok.integration.tinvest.api

import dev.stok.common.kotlinx.coroutines.ComputingRateLimiter
import dev.stok.common.kotlinx.coroutines.JoinedRateLimiter
import dev.stok.common.kotlinx.coroutines.RateLimiter
import dev.stok.enum.InstrumentIdType
import dev.stok.integration.api.InstrumentApi
import dev.stok.integration.model.InstrumentInfo
import dev.stok.integration.stereotype.CoroutineScopeAware
import dev.stok.integration.tinvest.JvmParameters
import dev.stok.integration.tinvest.SUPPORTED_INSTRUMENT_ID_TYPES
import dev.stok.integration.tinvest.UID_CUSTOM_INSTRUMENT_TYPE
import dev.stok.integration.tinvest.enum.TradeClassCode
import dev.stok.integration.tinvest.util.computeRateLimitRefillTime
import dev.stok.integration.tinvest.util.rpsServiceLimiter
import dev.stok.stereotype.StatefulElement
import dev.stok.typing.CustomInstrumentId
import dev.stok.typing.Figi
import dev.stok.typing.FigiValue
import dev.stok.typing.InstrumentId
import dev.stok.typing.Isin
import dev.stok.typing.Ticker
import dev.stok.typing.TickerValue
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.future.await
import kotlinx.datetime.Clock
import ru.tinkoff.piapi.core.InstrumentsService
import ru.tinkoff.piapi.core.InvestApi
import kotlin.coroutines.cancellation.CancellationException

private typealias TinvestInstrument = ru.tinkoff.piapi.contract.v1.Instrument
private typealias TinvestInstrumentShort = ru.tinkoff.piapi.contract.v1.InstrumentShort

class TinvestInstrumentApi(
    private val token: String
) : InstrumentApi, CoroutineScopeAware, StatefulElement<TinvestInstrumentApi> {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> by ::SUPPORTED_INSTRUMENT_ID_TYPES

    private val instrumentService: InstrumentsService = InvestApi.create(token).instrumentsService

    private lateinit var instrumentServiceRateLimiter: RateLimiter

    override fun setCoroutineScope(scope: CoroutineScope) {
        this.instrumentServiceRateLimiter = with(scope) {
            JoinedRateLimiter(
                ComputingRateLimiter(
                    limit = JvmParameters.instrumentsServiceRpmLimit,
                    refillMarkFn = { computeRateLimitRefillTime(Clock.System) },
                ),
                rpsServiceLimiter(),
            )
        }
    }

    override suspend fun getInstrumentInfo(instrumentId: InstrumentId): InstrumentInfo {
        val domainInstrument = getDomainInstrumentInfo(instrumentId)
        return InstrumentInfo(
            displayName = domainInstrument.name,
            currency = domainInstrument.currency.uppercase(),
            exchange = domainInstrument.exchange,
            figi = domainInstrument.figi,
            isin = domainInstrument.isin,
            ticker = domainInstrument.ticker,
        )
    }

    override suspend fun isExistingInstrument(instrumentId: InstrumentId): Boolean =
        try {
            findInstrument(id = instrumentId.value).isNotEmpty()
        } catch (ex: Throwable) {
            throw LOGGER.throwing(RuntimeException("Could not get instrument info for [$instrumentId]", ex))
        }

    override suspend fun convertInstrumentId(instrumentId: InstrumentId, targetType: InstrumentIdType): InstrumentId {
        require((instrumentId.type in supportedInstrumentIdTypes) && (targetType in supportedInstrumentIdTypes)) {
            "Cannot convert from type [${instrumentId.type}] to [$targetType]"
        }

        // Fast path
        if (instrumentId.type == targetType) {
            return instrumentId
        }

        val domainInstrument = getDomainInstrumentInfo(instrumentId)
        return when (targetType) {
            InstrumentIdType.Figi -> Figi(domainInstrument.figi)
            InstrumentIdType.Ticker -> Ticker(domainInstrument.ticker)
            InstrumentIdType.Isin -> Isin(domainInstrument.isin)
            UID_CUSTOM_INSTRUMENT_TYPE -> CustomInstrumentId(UID_CUSTOM_INSTRUMENT_TYPE, domainInstrument.uid)
            else -> error("Instrument type [$targetType] is not supported")
        }
    }

    private suspend fun getDomainInstrumentInfo(instrumentId: InstrumentId): TinvestInstrument =
        try {
            instrumentServiceRateLimiter.takePermit()
            when (instrumentId) {
                is Ticker -> getInstrumentByTicker(instrumentId.value)
                is Figi -> getInstrumentByFigi(instrumentId.value)
                is Isin -> {
                    val instrument = findInstrument(id = instrumentId.value)
                        .first { it.isin == instrumentId.value }
                    getInstrumentByFigi(instrument.figi)
                }

                is CustomInstrumentId -> {
                    check(instrumentId.type == UID_CUSTOM_INSTRUMENT_TYPE) {
                        "Instrument id type [${instrumentId.type}] is not supported"
                    }

                    val instrument = findInstrument(id = instrumentId.value)
                        .first { it.isin == instrumentId.value }
                    getInstrumentByFigi(instrument.figi)
                }
            }
        } catch (ex: Throwable) {
            if (ex is CancellationException) throw ex
            throw LOGGER.throwing(RuntimeException("Could not get instrument info for [$instrumentId]", ex))
        }

    private suspend fun getInstrumentByTicker(value: TickerValue): TinvestInstrument {
        instrumentServiceRateLimiter.takePermit()
        return instrumentService.getInstrumentByTicker(
            /* ticker = */ value,
            /* classCode = */ TradeClassCode.CENTRAL_ORDER_BOOK.code,
        ).await()
    }

    private suspend fun getInstrumentByFigi(value: FigiValue): TinvestInstrument {
        instrumentServiceRateLimiter.takePermit()
        return instrumentService.getInstrumentByFigi(value).await()
    }

    private suspend fun findInstrument(id: String): List<TinvestInstrumentShort> {
        instrumentServiceRateLimiter.takePermit()
        return instrumentService.findInstrument(/* id = */ id).await()
    }

    override fun clone(): TinvestInstrumentApi =
        TinvestInstrumentApi(token = token)

    companion object {

        private val LOGGER: KLogger = KotlinLogging.logger {}
    }
}
