# Tinkoff Invest API Integration

Integration with [Tinkoff Invest API][tinvest-api-doc].

```kt
bootstrap {
    tinvest {
        token("<YOUR TOKEN>")
    }
}
```

## Configuration

The only required parameter is Tinkoff Invest API token provided with `token(String)`.
Other configurations are optional.

Access to market data is enabled by default. Use `enableMarketData(Boolean)` to disable.

Access to trading orders is disable by default. Use `enableTrading(Boolean)` to enable.

Access to accounts and portfolio are disabled by default. Use `enableReadonlyAccess(Boolean)` to enable.

### JVM parameters

| Parameter                                 | Description                                                                                     | Type  | Default value |
|-------------------------------------------|-------------------------------------------------------------------------------------------------|-------|---------------|
| `stok.tinvest.instrumentsServiceRpmLimit` | Maximal allowed requests per minute to [instrument service][tinvest-api-doc-instrumentService]  | `Int` | `200`         |
| `stok.tinvest.marketDataServiceRpmLimit`  | Maximal allowed requests per minute to [market data service][tinvest-api-doc-marketDataService] | `Int` | `300`         |
| `stok.tinvest.operationsServiceRpmLimit`  | Maximal allowed requests per minute to [operations service][tinvest-api-doc-operationsService]  | `Int` | `200`         |
| `stok.tinvest.ordersServiceRpmLimit`      | Maximal allowed requests per minute to [order service][tinvest-api-doc-orderService]            | `Int` | `100`         |
| `stok.tinvest.usersServiceRpmLimit`       | Maximal allowed requests per minute to [user service][tinvest-api-doc-userService]              | `Int` | `100`         |

Default values for maximal requests per minute were set according to [Tinkoff Invest Api. Limits policy of Tinkoff Invest Api][tinvest-api-doc-limits] 

[tinvest-api-doc]: https://tinkoff.github.io/investAPI/
[tinvest-api-doc-userService]: https://tinkoff.github.io/investAPI/head-users/
[tinvest-api-doc-instrumentService]: https://tinkoff.github.io/investAPI/head-instruments/
[tinvest-api-doc-marketDataService]: https://tinkoff.github.io/investAPI/head-marketdata/
[tinvest-api-doc-operationsService]: https://tinkoff.github.io/investAPI/head-operations/
[tinvest-api-doc-orderService]: https://tinkoff.github.io/investAPI/head-orders/
[tinvest-api-doc-limits]: https://tinkoff.github.io/investAPI/limits/
