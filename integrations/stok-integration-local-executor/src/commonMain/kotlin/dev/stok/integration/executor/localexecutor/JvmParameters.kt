package dev.stok.integration.executor.localexecutor

object JvmParameters {

    val parallelEventsPartitionHandling: Boolean =
        System.getProperty("stok.local.parallelEventsPartitionHandling", "false").toBoolean()

    val sliceEventsPartitionOnEachInterrupt: Boolean =
        System.getProperty("stok.local.sliceEventsPartitionOnEachInterrupt", "true").toBoolean()
}
