package dev.stok.integration

import dev.stok.integration.context.BootstrapBuilderContext
import dev.stok.integration.executor.localexecutor.api.LocalExecutorApi

fun BootstrapBuilderContext.localExecutor() {
    val context: BootstrapBuilderContext = this@localExecutor

    context.setByName(BootstrapStandardNames.executorApi, LocalExecutorApi())
}
