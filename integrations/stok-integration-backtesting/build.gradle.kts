plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.stokIntegrationModel)
            }
        }
    }
}
