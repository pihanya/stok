package dev.stok.integration.backtesting

import dev.stok.integration.api.AccountApi
import dev.stok.integration.enum.AccountAccessLevel
import dev.stok.integration.enum.AccountStatus
import dev.stok.integration.model.StokAccountDetails
import dev.stok.integration.model.StokAccountSummary
import dev.stok.model.StokAccount

internal const val ACCOUNT_ID: String = "backtesting"

fun checkAccount(account: StokAccount) = checkAccount(account.id)

fun checkAccount(accountId: String) {
    check(accountId == ACCOUNT_ID) { "Account id [$accountId] is not supported for backtesting" }
}

internal val ACCOUNT: StokAccountSummary = StokAccountSummary(id = ACCOUNT_ID, name = "Backtesting Account")

internal val ACCOUNT_DETAILS: StokAccountDetails = StokAccountDetails(
    id = ACCOUNT_ID,
    name = ACCOUNT.name,
    AccountStatus.ACTIVE,
    AccountAccessLevel.FULL_ACCESS,
)

class BacktestingAccountApi : AccountApi {

    override suspend fun getAccounts(): List<StokAccountSummary> =
        listOf(ACCOUNT)

    override suspend fun getAccountById(id: String): StokAccountDetails {
        check(id == ACCOUNT_ID) { "Account id [$id] is not supported in backtesting" }
        return ACCOUNT_DETAILS
    }
}
