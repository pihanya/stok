package dev.stok.integration.backtesting.enum

enum class FileType { CSV, JSON }
