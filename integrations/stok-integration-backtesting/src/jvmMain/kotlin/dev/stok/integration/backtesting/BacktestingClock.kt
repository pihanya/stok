package dev.stok.integration.backtesting

import kotlinx.datetime.Clock
import kotlinx.datetime.Instant

class BacktestingClock : Clock {

    private lateinit var currentTime: Instant

    override fun now(): Instant = currentTime

    fun setCurrentTime(value: Instant) {
        this.currentTime = value
    }

    fun isInitialized(): Boolean =
        ::currentTime.isInitialized
}
