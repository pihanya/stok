package dev.stok.integration.backtesting.model

import dev.stok.integration.backtesting.enum.FileType
import dev.stok.typing.InstrumentId
import java.nio.file.Path

data class CandleSourceDefinition(
    val filePath: Path,
    val instumentId: InstrumentId?,
    val fileType: FileType
)
