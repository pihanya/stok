package dev.stok.integration.backtesting

import dev.stok.enum.InstrumentIdType
import dev.stok.integration.api.OrderApi
import dev.stok.model.MarketOrder
import dev.stok.model.StokAccount
import dev.stok.model.StokOrder
import dev.stok.typing.InstrumentId
import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity

class BacktestingOrderApi(private val memory: BacktestingMemory) : OrderApi {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> = setOf(InstrumentIdType.Ticker)

    override suspend fun marketBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder {
        checkAccount(account)
        TODO("Not yet implemented")
    }

    override suspend fun marketSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder {
        checkAccount(account)
        TODO("Not yet implemented")
    }

    override suspend fun limitBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): Nothing =
        throw NotImplementedError("Limit orders not supported in backtesting")

    override suspend fun limitSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): Nothing =
        throw NotImplementedError("Limit orders not supported in backtesting")

    override suspend fun getActiveOrders(account: StokAccount): List<StokOrder> {
        TODO("Not yet implemented")
    }

    override suspend fun cancelOrder(order: StokOrder) {
        throw NotImplementedError("Cancelling order not supported in backtesting")
    }
}
