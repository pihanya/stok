package dev.stok.integration.backtesting.typing

import dev.stok.common.typing.DecimalValue
import dev.stok.typing.CandleInterval
import dev.stok.typing.HistoricCandle
import dev.stok.typing.Quantity
import dev.stok.typing.TimeAwareCandle
import kotlinx.datetime.Instant

fun backtestingCandle(
    value: DecimalValue,
    interval: CandleInterval,
    openTime: Instant,
    volume: Quantity = 10
): TimeAwareCandle = HistoricCandle(
    open = value,
    close = value,
    low = value,
    high = value,
    volume = volume,
    interval = interval,
    openTime = openTime,
)
