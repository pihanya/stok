package dev.stok.integration.adhoc

import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.enum.InstrumentIdType
import dev.stok.integration.adhoc.typing.adHocCandle
import dev.stok.integration.api.QuoteApi
import dev.stok.integration.model.CandleRequest
import dev.stok.integration.model.OrderBook
import dev.stok.typing.InstrumentId
import dev.stok.typing.TimeAwareCandle
import kotlin.random.Random

class AdHocQuoteApi : QuoteApi {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> = setOf(InstrumentIdType.Ticker)

    override suspend fun getCandles(request: CandleRequest): List<TimeAwareCandle> {
        val prices = listOf(
            165.19, // Yahoo, APPL, 6m, 04/27, 09:30AM
            167.29,
            168.49,
            167.76,
            169.28,
            170.13,
            169.09,
            168.8,
            169.5,
            170.16,
        )

        return buildList list@{
            var index = 0
            var moment = request.from
            while (moment <= request.to) {
                val basePrice = prices[index % prices.size]
                val percent = Random.nextInt(-20, 20)

                val price = (basePrice * (1 + (percent.toDouble() / 100))).toDecimal()
                this@list += adHocCandle(price, request.interval, moment)
                moment += request.interval
                ++index
            }
        }
    }

    override suspend fun getOrderBook(instrumentId: InstrumentId, depth: Short): OrderBook {
        TODO("Not yet implemented")
    }
}
