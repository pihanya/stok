package dev.stok.integration.adhoc

import dev.stok.enum.InstrumentIdType
import dev.stok.integration.api.OrderApi
import dev.stok.model.LimitOrder
import dev.stok.model.MarketOrder
import dev.stok.model.StokAccount
import dev.stok.model.StokOrder
import dev.stok.typing.InstrumentId
import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity

class AdHocOrderApi : OrderApi {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> = setOf(InstrumentIdType.Ticker)

    override suspend fun marketBuy(instrumentId: InstrumentId, quantity: Quantity, account: StokAccount): MarketOrder {
        TODO("Not yet implemented")
    }

    override suspend fun marketSell(instrumentId: InstrumentId, quantity: Quantity, account: StokAccount): MarketOrder {
        TODO("Not yet implemented")
    }

    override suspend fun limitBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder {
        TODO("Not yet implemented")
    }

    override suspend fun limitSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder {
        TODO("Not yet implemented")
    }

    override suspend fun getActiveOrders(account: StokAccount): List<StokOrder> {
        TODO("Not yet implemented")
    }

    override suspend fun cancelOrder(order: StokOrder) {
        TODO("Not yet implemented")
    }
}
