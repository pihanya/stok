package dev.stok.enum

enum class TriggerState {

    WAITING,

    SCHEDULED,

    RUNNING,

    FINISHED
}
