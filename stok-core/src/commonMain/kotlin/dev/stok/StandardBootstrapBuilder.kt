package dev.stok

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.integration.BootstrapStandardNames
import dev.stok.integration.context.BootstrapBuilderContext
import dev.stok.model.MapBasedNamedValueContainer
import dev.stok.model.StandardStokBootstrap
import dev.stok.stereotype.MutableNamedValueContainer
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.datetime.Clock
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

@InternalLibraryApi
internal class StandardBootstrapBuilder :
    StokBootstrapBuilder,
    BootstrapBuilderContext,
    MutableNamedValueContainer<Any> by MapBasedNamedValueContainer() {

    override val parameters: MutableMap<String, Any> = HashMap()

    private val bootstrap: StandardStokBootstrap = StandardStokBootstrap()

    override fun build(): StandardStokBootstrap {
        bootstrap[BootstrapStandardNames.coroutineScopeSupplier] = ::createCoroutineScope
        bootstrap[BootstrapStandardNames.logger] = KotlinLogging.logger(STOK_LOGGER_NAME)
        bootstrap[BootstrapStandardNames.clock] = Clock.System

        bootstrap[BootstrapStandardNames.parametersContainer] = MapBasedNamedValueContainer(parameters)
        bootstrap[BootstrapStandardNames.variablesContainer] = MapBasedNamedValueContainer<Any>()

        for ((key, element) in this@StandardBootstrapBuilder.toMap()) {
            bootstrap[key] = element
        }

        return bootstrap
    }

    companion object {

        private val LOGGER: KLogger = KotlinLogging.logger {}

        private fun createCoroutineScope(): CoroutineScope {
            val dispatcher = Dispatchers.Default
            val job = SupervisorJob()
            val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
                LOGGER.error(throwable) { "Coroutine failed with exception" }
            }

            val coroutineContext = EmptyCoroutineContext
                .plus(dispatcher as CoroutineContext.Element)
                .plus(job as CoroutineContext.Element)
                .plus(exceptionHandler as CoroutineContext.Element)

            return CoroutineScope(coroutineContext)
        }
    }
}
