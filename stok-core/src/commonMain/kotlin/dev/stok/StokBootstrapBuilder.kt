package dev.stok

import dev.stok.common.definition.LibraryApi

@LibraryApi
interface StokBootstrapBuilder {

    fun build(): StokBootstrap
}
