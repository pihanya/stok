package dev.stok

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.dsl.context.LifecycleBuilderContext
import dev.stok.dsl.context.LifecycleEventContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.integration.event.LifecycleEvent.AfterStateChangeEvent
import dev.stok.integration.event.LifecycleEvent.BeforeStateChangeEvent
import dev.stok.model.TriggerRegistration
import dev.stok.trigger.AfterFinishTransitionTrigger
import dev.stok.trigger.AfterInterruptTransitionTrigger
import dev.stok.trigger.AfterPauseTransitionTrigger
import dev.stok.trigger.AfterStartTransitionTrigger
import dev.stok.trigger.BeforeFinishTransitionTrigger
import dev.stok.trigger.BeforeInterruptTransitionTrigger
import dev.stok.trigger.BeforePauseTransitionTrigger
import dev.stok.trigger.BeforeStartTransitionTrigger

@InternalLibraryApi
internal class StandardLifecycleBuilder(
    private val strategy: StokStrategy
) : LifecycleBuilderContext {

    override fun beforeStart(
        action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(BeforeStartTransitionTrigger(action))

    override fun afterStart(
        action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(AfterStartTransitionTrigger(action))

    override fun beforePause(
        action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(BeforePauseTransitionTrigger(action))

    override fun afterPause(
        action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(AfterPauseTransitionTrigger(action))

    override fun beforeResume(
        action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(BeforePauseTransitionTrigger(action))

    override fun afterResume(
        action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(AfterPauseTransitionTrigger(action))

    override fun beforeInterrupt(
        action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(BeforeInterruptTransitionTrigger(action))

    override fun afterInterrupt(
        action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(AfterInterruptTransitionTrigger(action))

    override fun beforeFinish(
        action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(BeforeFinishTransitionTrigger(action))

    override fun afterFinish(
        action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
    ): TriggerRegistration =
        strategy.registerTrigger(AfterFinishTransitionTrigger(action))
}
