package dev.stok.model

import dev.stok.StokBootstrap
import dev.stok.StokStrategy
import dev.stok.StokStrategyManager
import dev.stok.common.definition.InternalLibraryApi
import dev.stok.common.stereotype.DistributedField
import dev.stok.common.util.forkIf
import dev.stok.dsl.kotlin.extension.executorApi
import dev.stok.enum.StrategyState
import dev.stok.integration.internal.model.StokExecutionState
import dev.stok.integration.internal.stereotype.ExecutionStateAware
import io.github.oshai.kotlinlogging.KLogger

@Suppress("TooManyFunctions")
@InternalLibraryApi
class StandardStokStrategyManager(
    bootstrap: StokBootstrap,
    strategy: StokStrategy
) : StokStrategyManager, ExecutionStateAware {

    override val state: StrategyState get() = stateField.localValue

    override val strategy: StokStrategy = strategy.clone()

    override val bootstrap: StokBootstrap = bootstrap.clone()

    private lateinit var executionState: StokExecutionState

    private val stateField: DistributedField<StrategyState>
        get() = executionState.stateField

    override fun setExecutionState(value: StokExecutionState) {
        this.executionState = value
    }

    override fun start(wait: Boolean) {
        LOGGER.debug { "Strategy start was requested" }

        val alreadyStarted = ::executionState.isInitialized
        if (alreadyStarted) return

        forkIf(!wait) {
            // start() phase #1
            LOGGER.debug { "start() phase #1" }
            bootstrap.executorApi.schedule(this@StandardStokStrategyManager)
            LOGGER.info { "Strategy was successfully scheduled" }

            forkIf(!wait) {
                LOGGER.debug { "start() phase #2" }
                bootstrap.executorApi.start()
                LOGGER.info { "Strategy was successfully started" }
            }
        }
    }

    override fun pause(wait: Boolean) {
        LOGGER.debug { "Strategy pause was requested" }

        val alreadyPaused = state == StrategyState.PAUSED
        if (alreadyPaused) {
            LOGGER.debug { "Strategy was already paused" }
            return
        }

        forkIf(!wait) {
            bootstrap.executorApi.pause()
            LOGGER.info { "Strategy was successfully paused" }
        }
    }

    override fun resume(wait: Boolean) {
        LOGGER.debug { "Strategy resume was requested" }

        val alreadyRunning = state in setOf(StrategyState.PARKED, StrategyState.RUNNING)
        if (alreadyRunning) {
            LOGGER.debug { "Strategy was already running" }
            return
        }

        forkIf(!wait) {
            bootstrap.executorApi.resume()
            LOGGER.info { "Strategy was successfully resumed" }
        }
    }

    override fun shutdown(wait: Boolean) {
        val alreadyShutdown = state in setOf(StrategyState.FINISHING, StrategyState.FINISHED)
        if (alreadyShutdown) {
            LOGGER.debug { "Strategy was already shutdown" }
            return
        }

        forkIf(!wait) {
            bootstrap.executorApi.shutdown()
            LOGGER.info { "Strategy was successfully finished" }
        }
    }

    companion object {

        val LOGGER: KLogger = StokStrategyManager.LOGGER
    }
}
