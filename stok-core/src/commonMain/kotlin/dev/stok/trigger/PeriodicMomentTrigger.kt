@file:Suppress("ktlint") // ktlint tries to add additional indent which looks awful

package dev.stok.trigger

import dev.stok.context.TimeAwareContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.dsl.typing.PeriodicMoment
import dev.stok.stereotype.ActivationTimeAware
import dev.stok.stereotype.StatefulElement
import kotlinx.datetime.Instant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

/**
 * @see dev.stok.dsl.context.StrategyBuilderContext.atMoment
 */
class PeriodicMomentTrigger(
    private val moment: PeriodicMoment,
    private val triggerConstraints: Duration = 1.seconds,
    action: DslAction<StrategyContext>
) : AbstractActionTrigger<StrategyContext>(action, StrategyContext::class),
    ActivationTimeAware,
    StatefulElement<PeriodicMomentTrigger> {

    private var lastTriggeredTime: Instant = Instant.DISTANT_PAST

    init {
        require(!triggerConstraints.isNegative()) {
            "Trigger constrains [$triggerConstraints] must not be negative"
        }
    }

    override suspend fun afterTrigger(context: StrategyContext) {
        lastTriggeredTime = context.currentTime
    }

    override fun nextActivationTime(context: TimeAwareContext): Instant {
        val currentTime = context.currentTime
        val triggeredOnce = (lastTriggeredTime != Instant.DISTANT_PAST)
        val sincePrev = moment.computeSincePreviousActivation(currentTime)
        val untilNext = moment.computeUntilNextActivation(currentTime)

        val prevTriggerTime = currentTime - sincePrev
        val futureTriggerTime = currentTime + untilNext

        return when {
            (sincePrev > triggerConstraints) -> {
                if (triggeredOnce && lastTriggeredTime < prevTriggerTime) {
                    lastTriggeredTime = prevTriggerTime
                    LOGGER.debug { "Activation was skipped at $prevTriggerTime" }
                }
                futureTriggerTime
            }

            !triggeredOnce -> currentTime
            triggeredOnce && (lastTriggeredTime < prevTriggerTime) -> currentTime
            else -> futureTriggerTime
        }
    }

    override fun clone(): PeriodicMomentTrigger =
        PeriodicMomentTrigger(
            moment = moment,
            triggerConstraints = triggerConstraints,
            action = action,
        )
}
