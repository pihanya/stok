@file:Suppress("ktlint") // ktlint tries to add additional indent which looks awful

package dev.stok.trigger

import dev.stok.context.TimeAwareContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.stereotype.ActivationTimeAware
import dev.stok.stereotype.StatefulElement
import kotlinx.datetime.Instant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

/**
 * @see dev.stok.context.StrategyBuilderContext.everyPeriod
 */
class PeriodicTrigger(
    private val period: Duration,
    private val triggerOnStart: Boolean,
    private val triggerConstraints: Duration = 500.milliseconds,
    action: DslAction<StrategyContext>
) : AbstractActionTrigger<StrategyContext>(action, StrategyContext::class),
    ActivationTimeAware,
    StatefulElement<PeriodicTrigger> {

    private lateinit var firstCheckTime: Instant

    private var lastTriggeredTime: Instant = Instant.DISTANT_PAST

    init {
        require(period.isPositive()) {
            "Period [$period] must not be positive"
        }
        require(period.isFinite()) {
            "Period [$period] must not be finite"
        }
        require(!triggerConstraints.isNegative()) {
            "Trigger constrains [$triggerConstraints] must not be negative"
        }
    }

    override suspend fun afterTrigger(context: StrategyContext) {
        lastTriggeredTime = context.currentTime
    }

    override fun nextActivationTime(context: TimeAwareContext): Instant {
        val currentTime = context.currentTime

        if (::firstCheckTime.isInitialized.not()) {
            firstCheckTime = currentTime
        }

        val triggeredOnce = (lastTriggeredTime != Instant.DISTANT_PAST)
        return if (!triggeredOnce) {
            val expectedTriggerTime = firstCheckTime + period
            when {
                triggerOnStart -> currentTime
                currentTime < expectedTriggerTime -> expectedTriggerTime
                (currentTime - expectedTriggerTime) <= triggerConstraints -> currentTime
                else -> {
                    lastTriggeredTime = expectedTriggerTime
                    LOGGER.debug { "Activation was skipped at $expectedTriggerTime" }

                    var futureTriggerTime = expectedTriggerTime
                    while (futureTriggerTime < currentTime) {
                        futureTriggerTime += period
                    }
                    futureTriggerTime
                }
            }
        } else {
            val expectedTriggerTime = lastTriggeredTime + period
            when {
                currentTime < expectedTriggerTime -> expectedTriggerTime
                (currentTime - expectedTriggerTime) <= triggerConstraints -> currentTime
                else -> {
                    lastTriggeredTime = expectedTriggerTime
                    LOGGER.debug { "Activation was skipped at $expectedTriggerTime" }

                    var futureTriggerTime = expectedTriggerTime
                    while (futureTriggerTime < currentTime) {
                        futureTriggerTime += period
                    }
                    futureTriggerTime
                }
            }
        }
    }

    override fun clone(): PeriodicTrigger =
        PeriodicTrigger(
            period = period,
            triggerOnStart = triggerOnStart,
            triggerConstraints = triggerConstraints,
            action = action,
        )
}
