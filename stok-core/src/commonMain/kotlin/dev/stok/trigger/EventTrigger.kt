package dev.stok.trigger

import dev.stok.common.util.uncheckedCast
import dev.stok.context.EventAwareContext
import dev.stok.dsl.context.StrategyEventContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.stereotype.StokContext
import dev.stok.stereotype.StokEvent
import kotlin.reflect.KClass

/**
 * @see dev.stok.dsl.context.StrategyBuilderContext.onEvent
 */
class EventTrigger<E : StokEvent>(
    private val eventClass: KClass<E>,
    action: DslAction<StrategyEventContext<E>>
) : AbstractActionTrigger<StrategyEventContext<E>>(action, StrategyEventContext::class) {

    override fun isTriggered(context: StokContext): Boolean {
        if (!super.isTriggered(context)) return false
        return eventClass.isInstance(uncheckedCast<EventAwareContext>(context).event)
    }
}
