package dev.stok.integration.event

import dev.stok.integration.model.CandleRequest
import dev.stok.stereotype.StokEvent
import dev.stok.typing.Candle
import dev.stok.typing.InstrumentId
import kotlinx.datetime.Instant

interface QuoteApiEvent : StokEvent {

    val instrumentId: InstrumentId
}

class GetCandlesResponseEvent(
    val request: CandleRequest,
    val candles: List<Candle>
) : QuoteApiEvent {

    override lateinit var creationTime: Instant

    override lateinit var dispatchTime: Instant

    override val instrumentId: InstrumentId = request.instrumentId
}
