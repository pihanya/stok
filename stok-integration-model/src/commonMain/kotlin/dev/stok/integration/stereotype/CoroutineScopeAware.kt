package dev.stok.integration.stereotype

import kotlinx.coroutines.CoroutineScope

interface CoroutineScopeAware {

    fun setCoroutineScope(scope: CoroutineScope)
}
