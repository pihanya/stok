package dev.stok.integration

import dev.stok.common.definition.LibraryApi

@LibraryApi
object BootstrapStandardNames {

    /**
     * Type: `io.github.oshai.kotlinlogging.KLogger`
     */
    const val logger: String = "logger"

    /**
     * Type: `kotlinx.datetime.Clock`
     */
    const val clock: String = "clock"

    /**
     * Type: `() -> kotlinx.coroutines.CoroutineScope`
     */
    const val coroutineScopeSupplier: String = "coroutineScopeSupplier"

    /**
     * Type: `NamedValueContainer<Any>`
     */
    const val parametersContainer: String = "parametersContainer"

    /**
     * Type: `MutableNamedValueContainer<Any>`
     */
    const val variablesContainer: String = "variablesContainer"

    /**
     * @see dev.stok.integration.api.AccountApi
     */
    const val accountApi: String = "accountApi"

    /**
     * @see dev.stok.integration.api.PortfolioApi
     */
    const val portfolioApi: String = "operationApi"

    /**
     * @see dev.stok.integration.api.InstrumentApi
     */
    const val instrumentApi: String = "instrumentApi"

    /**
     * @see dev.stok.integration.api.OrderApi
     */
    const val orderApi: String = "orderApi"

    /**
     * @see dev.stok.integration.api.ExecutorApi
     */
    const val executorApi: String = "executorApi"

    /**
     * @see dev.stok.integration.api.QuoteApi
     */
    const val quoteApi: String = "quoteApi"
}
