package dev.stok.integration.enum

enum class AccountStatus {

    UNKNOWN,

    CREATED,

    ACTIVE,

    CLOSED
}
