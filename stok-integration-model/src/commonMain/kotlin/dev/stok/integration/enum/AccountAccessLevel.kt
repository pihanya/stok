package dev.stok.integration.enum

enum class AccountAccessLevel {

    UNKNOWN,

    NO_ACCESS,

    READONLY,

    FULL_ACCESS
}
