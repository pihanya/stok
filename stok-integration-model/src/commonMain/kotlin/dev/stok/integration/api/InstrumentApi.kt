package dev.stok.integration.api

import dev.stok.enum.InstrumentIdType
import dev.stok.integration.model.InstrumentInfo
import dev.stok.integration.stereotype.StokApi
import dev.stok.typing.InstrumentId

interface InstrumentApi : StokApi {

    val supportedInstrumentIdTypes: Set<InstrumentIdType>

    suspend fun getInstrumentInfo(instrumentId: InstrumentId): InstrumentInfo

    suspend fun isExistingInstrument(instrumentId: InstrumentId): Boolean

    suspend fun convertInstrumentId(instrumentId: InstrumentId, targetType: InstrumentIdType): InstrumentId
}
