package dev.stok.integration.api

import dev.stok.enum.InstrumentIdType
import dev.stok.integration.stereotype.StokApi
import dev.stok.model.LimitOrder
import dev.stok.model.MarketOrder
import dev.stok.model.StokAccount
import dev.stok.model.StokOrder
import dev.stok.typing.InstrumentId
import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity

interface OrderApi : StokApi {

    val supportedInstrumentIdTypes: Set<InstrumentIdType>

    suspend fun marketBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder

    suspend fun marketSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder

    suspend fun limitBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder

    suspend fun limitSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder

    suspend fun getActiveOrders(account: StokAccount): List<StokOrder>

    suspend fun cancelOrder(order: StokOrder)
}
