package dev.stok.integration.api

import dev.stok.StokStrategyManager
import dev.stok.integration.internal.event.BatchEventSource
import dev.stok.integration.stereotype.StokApi
import dev.stok.stereotype.StokEvent

interface ExecutorApi : StokApi, BatchEventSource<StokEvent> {

    suspend fun schedule(manager: StokStrategyManager)

    suspend fun start()

    suspend fun pause()

    suspend fun resume()

    suspend fun shutdown()
}
