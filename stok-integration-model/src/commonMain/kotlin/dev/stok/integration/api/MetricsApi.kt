package dev.stok.integration.api

import dev.stok.integration.stereotype.StokApi

interface MetricsApi : StokApi {

    fun addTrade()
}
