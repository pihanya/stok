package dev.stok.integration.api

import dev.stok.integration.model.StokAccountDetails
import dev.stok.integration.model.StokAccountSummary
import dev.stok.integration.stereotype.StokApi

interface AccountApi : StokApi {

    suspend fun getAccounts(): List<StokAccountSummary>

    suspend fun getAccountById(id: String): StokAccountDetails
}
