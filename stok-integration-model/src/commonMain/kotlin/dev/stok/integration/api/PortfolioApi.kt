package dev.stok.integration.api

import dev.stok.integration.stereotype.StokApi
import dev.stok.model.StokAccount
import dev.stok.model.StokPosition
import dev.stok.typing.CurrencyCode
import dev.stok.typing.MoneyValue

interface PortfolioApi : StokApi {

    suspend fun getAllMoney(account: StokAccount): List<MoneyValue>

    suspend fun getMoney(currency: CurrencyCode, account: StokAccount): MoneyValue

    suspend fun getPositions(account: StokAccount): List<StokPosition>
}
