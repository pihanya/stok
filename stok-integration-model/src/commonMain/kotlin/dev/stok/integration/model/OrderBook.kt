package dev.stok.integration.model

data class OrderBook(
    val asks: List<OrderBookOrder>,
    val bids: List<OrderBookOrder>
)
