package dev.stok.integration.model

import dev.stok.model.StokAccount

data class StokAccountSummary(
    override val id: String,
    val name: String
) : StokAccount
