package dev.stok.integration.model

import dev.stok.common.typing.DecimalValue
import dev.stok.typing.Quantity

data class OrderBookOrder(
    val price: DecimalValue,
    val quantity: Quantity
)
