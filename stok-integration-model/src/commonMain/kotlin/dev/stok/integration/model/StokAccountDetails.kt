package dev.stok.integration.model

import dev.stok.integration.enum.AccountAccessLevel
import dev.stok.integration.enum.AccountStatus
import dev.stok.model.StokAccount

data class StokAccountDetails(
    override val id: String,
    val name: String,
    val status: AccountStatus,
    val accessLevel: AccountAccessLevel
) : StokAccount
