package dev.stok.integration.internal.event

interface EventBus<E : Any> : EventListener<E>, BatchEventSource<E> {

    fun emitEvents()
}
