package dev.stok.integration.internal.stereotype

import dev.stok.integration.internal.model.StokExecutionState

interface ExecutionStateAware {

    fun setExecutionState(value: StokExecutionState)
}
