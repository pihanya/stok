package dev.stok.integration.internal.event

import dev.stok.common.stereotype.Closeable

interface BatchEventSource<out E : Any> : Closeable {

    /**
     * Handler will always be called when at least one interruption event is triggered
     *
     * @param handler handler of events batches
     * @see dev.stok.integration.event.LifecycleEvent.InterruptEvent
     */
    fun subscribeOnEvents(handler: suspend (List<E>) -> Unit)
}
