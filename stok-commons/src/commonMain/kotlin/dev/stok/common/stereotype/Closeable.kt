package dev.stok.common.stereotype

interface Closeable {

    fun close()
}
