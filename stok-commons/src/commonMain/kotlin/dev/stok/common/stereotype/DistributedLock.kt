package dev.stok.common.stereotype

interface DistributedLock {

    suspend fun isLocked(): Boolean

    suspend fun lock()

    suspend fun unlock()
}

suspend inline fun <R> DistributedLock.withLock(block: () -> R): R {
    lock()
    return try {
        block()
    } finally {
        unlock()
    }
}
