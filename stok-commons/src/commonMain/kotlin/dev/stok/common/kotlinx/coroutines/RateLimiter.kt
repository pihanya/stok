package dev.stok.common.kotlinx.coroutines

import dev.stok.common.stereotype.Closeable
import dev.stok.common.util.delayUntilCondition
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.asTimeSource
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.TimeMark
import kotlin.time.TimeSource

@Suppress("NOTHING_TO_INLINE")
inline fun CoroutineScope.JoinedRateLimiter(vararg limiters: RateLimiter): RateLimiter =
    JoinedRateLimiter(limiters = limiters.toList(), scope = this@JoinedRateLimiter)

@Suppress("NOTHING_TO_INLINE")
inline fun CoroutineScope.DiscreteRateLimiter(
    limit: Int,
    period: Duration,
    filledInitially: Boolean = true,
    clock: Clock = Clock.System,
    checkDelay: Duration = DEFAULT_DELAY
): RateLimiter = DiscreteRateLimiter(
    limit = limit,
    period = period,
    filledInitially = filledInitially,
    checkDelay = checkDelay,
    clock = clock,
    scope = this@DiscreteRateLimiter,
)

@Suppress("NOTHING_TO_INLINE")
inline fun CoroutineScope.ComputingRateLimiter(
    limit: Int,
    filledInitially: Boolean = true,
    checkDelay: Duration = DEFAULT_DELAY,
    noinline refillMarkFn: () -> TimeMark
): RateLimiter = ComputingRateLimiter(
    limit = limit,
    refillMarkFn = refillMarkFn,
    filledInitially = filledInitially,
    checkDelay = checkDelay,
    scope = this@ComputingRateLimiter,
)

interface RateLimiter : Closeable {

    suspend fun takePermit() = takePermits(count = 1)

    suspend fun takePermits(count: Int)
}

@PublishedApi
internal val DEFAULT_DELAY: Duration = 10.milliseconds

@PublishedApi
internal class JoinedRateLimiter(
    private val limiters: List<RateLimiter>,
    private val scope: CoroutineScope
) : RateLimiter {

    override suspend fun takePermits(count: Int) = coroutineScope {
        limiters
            .map { limiter -> scope.launch { limiter.takePermits(count) } }
            .joinAll()
    }

    override fun close() {
        limiters.forEach(RateLimiter::close)
    }
}

@PublishedApi
internal class DiscreteRateLimiter(
    private val limit: Int,
    private val period: Duration,
    filledInitially: Boolean = true,
    private val checkDelay: Duration = DEFAULT_DELAY,
    clock: Clock,
    scope: CoroutineScope
) : RateLimiter {

    private val timeSource: TimeSource = clock.asTimeSource()

    private val permitsChannel: Channel<Unit> = Channel(
        capacity = limit,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    )

    private val fillingJob: Job = scope.launch {
        if (filledInitially) repeat(limit) { permitsChannel.send(Unit) }

        var nextTrigger: TimeMark = timeSource.markNow()
        while (isActive && !permitsChannel.isClosedForSend) {
            delayUntilCondition(waitDelay = checkDelay) { nextTrigger.hasPassedNow() }
            doSendPermit()
            nextTrigger += (period / limit)
        }
    }

    override suspend fun takePermits(count: Int) {
        check(!permitsChannel.isClosedForReceive) { "Bucket channel was closed" }
        repeat(count) { doTakePermit() }
    }

    private suspend fun doTakePermit() {
        permitsChannel.receive()
    }

    private suspend fun doSendPermit() {
        permitsChannel.send(Unit)
    }

    override fun close() {
        permitsChannel.close()
        fillingJob.cancel()
    }
}

@PublishedApi
internal class ComputingRateLimiter(
    private val limit: Int,
    refillMarkFn: () -> TimeMark,
    filledInitially: Boolean = true,
    private val checkDelay: Duration = DEFAULT_DELAY,
    scope: CoroutineScope
) : RateLimiter {

    private val permitsChannel: Channel<Unit> = Channel(
        capacity = limit,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
    )

    private val fillingJob: Job = scope.launch {
        if (filledInitially) repeat(limit) { permitsChannel.send(Unit) }

        var nextRefillMark = refillMarkFn()
        while (isActive && !permitsChannel.isClosedForSend) {
            delayUntilCondition(waitDelay = checkDelay) { nextRefillMark.hasPassedNow() }
            nextRefillMark = refillMarkFn()
            repeat(limit) { doSendPermit() }
        }
    }

    override suspend fun takePermits(count: Int) {
        check(!permitsChannel.isClosedForReceive) { "Bucket channel was closed" }
        delayUntilCondition { permitsChannel.isEmpty.not() }
        repeat(count) { doTakePermit() }
    }

    private suspend fun doTakePermit() {
        permitsChannel.receive()
    }

    private suspend fun doSendPermit() {
        permitsChannel.send(Unit)
    }

    override fun close() {
        permitsChannel.close()
        fillingJob.cancel()
    }
}
