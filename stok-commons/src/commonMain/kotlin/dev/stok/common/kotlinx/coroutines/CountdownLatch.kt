package dev.stok.common.kotlinx.coroutines

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CompletableJob
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

@Suppress("NOTHING_TO_INLINE")
inline fun CoroutineScope.CountDownLatch(value: Int): CountDownLatch =
    ScopeCountdownLatch(
        value = value,
        scope = this@CountDownLatch,
    )

interface CountDownLatch {

    suspend fun countDown()

    suspend fun await()

    fun onCompletion(block: suspend () -> Unit)
}

@PublishedApi
internal class ScopeCountdownLatch(
    value: Int = 1,
    scope: CoroutineScope
) : CountDownLatch {

    private val actorJob: CompletableJob = Job()

    private val actorChannel = scope.actor<LatchAction>(context = actorJob) {
        var counter: Int = value
        val plainSubscriptions: MutableList<CompletableDeferred<Unit>> = mutableListOf()
        val subscriptions: MutableList<suspend () -> Unit> = mutableListOf()

        while (isActive) {
            val action = channel.receive()
            when (action) {
                is LatchAction.Await -> {
                    plainSubscriptions += action.deferred
                }
                is LatchAction.SubscribeOnCompletion -> {
                    subscriptions += action.block
                }
                LatchAction.CountDown -> {
                    if (counter > 0) {
                        counter -= 1
                        if (counter == 0) {
                            plainSubscriptions.forEach { deferred -> deferred.complete(Unit) }
                            supervisorScope {
                                for (subscription in subscriptions) {
                                    launch { subscription() }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override suspend fun countDown() {
        actorChannel.send(LatchAction.CountDown)
    }

    override suspend fun await() {
        val deferred: CompletableDeferred<Unit> = CompletableDeferred()
        actorChannel.send(LatchAction.Await(deferred))
        deferred.await()
    }

    override fun onCompletion(block: suspend () -> Unit) {
        actorChannel.trySendBlocking(LatchAction.SubscribeOnCompletion(block))
            .getOrThrow()
    }

    private sealed interface LatchAction {

        object CountDown : LatchAction

        class Await(val deferred: CompletableDeferred<Unit>) : LatchAction

        class SubscribeOnCompletion(val block: suspend () -> Unit) : LatchAction
    }
}
