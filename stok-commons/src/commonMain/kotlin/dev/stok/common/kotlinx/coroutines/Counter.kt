package dev.stok.common.kotlinx.coroutines

import dev.stok.common.stereotype.Closeable
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CompletableJob
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.isActive

@Suppress("NOTHING_TO_INLINE")
inline fun CoroutineScope.Counter(initialValue: Long = 0L): Counter =
    ScopeCounter(
        initialValue = initialValue,
        scope = this@Counter,
    )

interface Counter : Closeable {

    suspend fun pushIncrement(value: Long)

    suspend fun getValue(): Long
}

@PublishedApi
internal class ScopeCounter(
    initialValue: Long = 0L,
    private val scope: CoroutineScope
) : Counter {

    private val actorJob: CompletableJob = Job()

    private val actorChannel: SendChannel<CounterAction> = scope.actor(context = actorJob) {
        var counter = initialValue
        while (isActive) {
            val action = channel.receive()
            when (action) {
                is CounterAction.Get -> {
                    action.deferred.complete(counter)
                }
                is CounterAction.Increment -> {
                    counter += action.value
                }
            }
        }
    }

    override suspend fun pushIncrement(value: Long) {
        check(scope.isActive) { "Counter scope was cancelled" }
        check(actorJob.isCompleted.not()) { "Counter job was completed" }

        actorChannel.send(CounterAction.Increment(value))
    }

    override suspend fun getValue(): Long {
        check(actorJob.isCompleted.not()) { "Counter job was completed" }

        val deferred: CompletableDeferred<Long> = CompletableDeferred()
        actorChannel.send(CounterAction.Get(deferred))
        return deferred.await()
    }

    override fun close() {
        actorJob.cancel()
    }

    private sealed interface CounterAction {

        class Get(val deferred: CompletableDeferred<Long>) : CounterAction

        class Increment(val value: Long) : CounterAction
    }
}
