package dev.stok.common.definition

/**
 * Annotation [InternalLibraryApi] is applied to source code
 * to indicate that the given code is exposed for module internal needs.
 * Source code of external modules (other domains) **MUST NOT** use source code marked with [InternalLibraryApi]
 * as backward compatibility for sources is not guaranteed.
 *
 * Developer **MUST** mark declaration with [InternalLibraryApi] annotation
 * if it is not published as API (simply saying — it is internal)
 *
 * Developer **SHOULD NOT** apply [InternalLibraryApi] annotation to functions and properties of a class
 * if [InternalLibraryApi] annotation is applied to the class.
 *
 * @author Mikhail Gostev
 * @see kotlinx.coroutines.InternalCoroutinesApi
 */
@Retention(value = AnnotationRetention.BINARY)
@Target(
    AnnotationTarget.CLASS,
    AnnotationTarget.FUNCTION,
    AnnotationTarget.TYPEALIAS,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.PROPERTY_SETTER,
)
@RequiresOptIn(
    level = RequiresOptIn.Level.WARNING,
    message = "This is an internal API that should not be used from outside of the library. " +
        "No compatibility guarantees are provided. " +
        "It is recommended to report your use-case of internal API to the owner's issue tracker, " +
        "so stable API could be provided instead",
)
annotation class InternalLibraryApi
