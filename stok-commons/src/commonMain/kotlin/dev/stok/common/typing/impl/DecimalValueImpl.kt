package dev.stok.common.typing.impl

import dev.stok.common.typing.DecimalValue

expect class DecimalValueImpl : DecimalValue {

    constructor(value: Number)

    constructor(value: String)

    override fun sqrt(): DecimalValue

    override fun toDouble(): Double

    override operator fun unaryPlus(): DecimalValue

    override operator fun unaryMinus(): DecimalValue

    override operator fun plus(other: DecimalValue): DecimalValue

    override operator fun minus(other: DecimalValue): DecimalValue

    override operator fun times(other: DecimalValue): DecimalValue

    override operator fun div(other: DecimalValue): DecimalValue

    override fun compareTo(other: Any): Int

    override fun equals(other: Any?): Boolean

    override fun hashCode(): Int

    override fun toString(): String
}
