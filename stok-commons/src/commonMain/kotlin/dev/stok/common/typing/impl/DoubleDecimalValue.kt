package dev.stok.common.typing.impl

import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.common.util.uncheckedCast
import kotlin.math.sqrt

private fun DecimalValue.unsafe(): DoubleDecimalValue = uncheckedCast(this)

class DoubleDecimalValue : DecimalValue {

    private val value: Double

    internal constructor(value: Double) {
        this.value = value
    }

    @Suppress("unused")
    constructor(value: Number) : this(value.toDouble())

    @Suppress("unused")
    constructor(value: String) : this(value.toDouble())

    override fun sqrt(): DecimalValue =
        DoubleDecimalValue(sqrt(value))

    override fun toDouble(): Double = value

    override operator fun unaryPlus(): DecimalValue = this

    override operator fun unaryMinus(): DecimalValue = DoubleDecimalValue(value.unaryMinus())

    override operator fun plus(other: DecimalValue): DecimalValue =
        DoubleDecimalValue(value.plus(other.unsafe().value))

    override operator fun minus(other: DecimalValue): DecimalValue =
        DoubleDecimalValue(value.minus(other.unsafe().value))

    override operator fun times(other: DecimalValue): DecimalValue =
        DoubleDecimalValue(value.times(other.unsafe().value))

    override operator fun div(other: DecimalValue): DecimalValue =
        DoubleDecimalValue(value.div(other.unsafe().value))

    override fun compareTo(other: Any): Int {
        val effectiveOther = when (other) {
            is DecimalValue -> other.unsafe()
            is Number -> other.toDecimal().unsafe()
            is String -> other.toDecimal().unsafe()
            else -> error("Unsupported comparison with instance of type [${other::class.simpleName}]")
        }
        return value.compareTo(effectiveOther.value)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as DecimalValue

        if (value != other.unsafe().value) return false

        return true
    }

    override fun hashCode(): Int =
        value.hashCode()

    override fun toString(): String =
        value.toString()
}
