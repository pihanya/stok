package dev.stok.common.typing.impl

import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.common.util.uncheckedCast
import korlibs.bignumber.BigNum

private fun DecimalValue.unsafe(): KBigNumDecimalValue = uncheckedCast(this)

class KBigNumDecimalValue private constructor(private val value: BigNum) : DecimalValue {

    @Suppress("unused")
    constructor(value: Number) : this(BigNum(value.toString()))

    @Suppress("unused")
    constructor(value: String) : this(BigNum(value))

    override fun sqrt(): DecimalValue =
        KBigNumDecimalValue(value.pow(exponent = -2, precision = 128))

    override fun toDouble(): Double =
        value.toString().toDouble()

    override operator fun unaryPlus(): DecimalValue =
        this

    override operator fun unaryMinus(): DecimalValue =
        KBigNumDecimalValue(value.times(BigNum.ZERO.minus(BigNum.ONE)))

    override operator fun plus(other: DecimalValue): DecimalValue =
        KBigNumDecimalValue(value.plus(other.unsafe().value))

    override operator fun minus(other: DecimalValue): DecimalValue =
        KBigNumDecimalValue(value.minus(other.unsafe().value))

    override operator fun times(other: DecimalValue): DecimalValue =
        KBigNumDecimalValue(value.times(other.unsafe().value))

    override operator fun div(other: DecimalValue): DecimalValue =
        KBigNumDecimalValue(value.div(other.unsafe().value))

    override fun compareTo(other: Any): Int {
        val effectiveOther = when (other) {
            is DecimalValue -> other.unsafe()
            is Number -> other.toDecimal().unsafe()
            is String -> other.toDecimal().unsafe()
            else -> error("Unsupported comparison with instance of type [${other::class.simpleName}]")
        }
        return value.compareTo(effectiveOther.value)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as DecimalValue

        if (value != other.unsafe().value) return false

        return true
    }

    override fun hashCode(): Int =
        value.hashCode()

    override fun toString(): String =
        value.toString()
}
