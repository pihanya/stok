package dev.stok.common.kotlin.extension

import dev.stok.common.typing.DecimalValue
import dev.stok.common.typing.impl.DecimalValueImpl

fun Number.toDecimal(): DecimalValue = DecimalValueImpl(this)

fun String.toDecimal(): DecimalValue = DecimalValueImpl(this)

fun DecimalValue.absolute(): DecimalValue =
    when {
        this == DecimalValue.ZERO -> DecimalValue.ZERO
        this < DecimalValue.ZERO -> unaryMinus()
        else -> this
    }

operator fun DecimalValue.plus(other: Number): DecimalValue =
    this.plus(other.toDecimal())

operator fun Number.plus(other: DecimalValue): DecimalValue =
    this.toDecimal().plus(other)

operator fun DecimalValue.minus(other: Number): DecimalValue =
    this.minus(other.toDecimal())

operator fun Number.minus(other: DecimalValue): DecimalValue =
    this.toDecimal().minus(other)

operator fun DecimalValue.times(other: Number): DecimalValue =
    this.times(other.toDecimal())

operator fun Number.times(other: DecimalValue): DecimalValue =
    this.toDecimal().times(other)

operator fun DecimalValue.div(other: Number): DecimalValue =
    this.div(other.toDecimal())

operator fun Number.div(other: DecimalValue): DecimalValue =
    this.toDecimal().div(other)
