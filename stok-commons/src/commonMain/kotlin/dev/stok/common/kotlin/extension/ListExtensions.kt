package dev.stok.common.kotlin.extension

fun <T> List<T>.partitionOn(condition: (partition: List<T>) -> Boolean): List<List<T>> {
    val list = this
    if (list.isEmpty()) return emptyList()

    val partitions = mutableListOf<List<T>>()

    val formingPartition = mutableListOf<T>()
    for (element in list) {
        formingPartition.add(element)

        val shouldSlice = condition(formingPartition)
        if (shouldSlice) {
            partitions.add(formingPartition.toList())
            formingPartition.clear()
        }
    }

    if (formingPartition.isNotEmpty()) {
        partitions.add(formingPartition.toList())
        formingPartition.clear()
    }

    return partitions
}
