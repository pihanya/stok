package dev.stok.common.util

import dev.stok.common.stereotype.DistributedField
import dev.stok.common.stereotype.DistributedLock

class LocalDistributedField<T>(initialValue: T) : DistributedField<T> {

    override var localValue: T = initialValue
        private set

    override val lock: DistributedLock = MutexDistributedLock()

    private val subscribers: MutableList<suspend (T, T) -> Unit> = mutableListOf()

    override suspend fun getActualValue(): T = localValue

    override suspend fun setNewValue(newValue: T) {
        val previousValue = localValue
        this.localValue = newValue
        handleChange(previousValue, newValue)
    }

    override fun subscribeOnChanges(handler: suspend (previousValue: T, currentValue: T) -> Unit) {
        this.subscribers += handler
    }

    private suspend fun handleChange(previousValue: T, currentValue: T) {
        for (subscriber in subscribers) {
            subscriber(previousValue, currentValue)
        }
    }
}
