package dev.stok.common.util

fun <T> T.identity(): T = this
