package dev.stok.common.util

import dev.stok.common.stereotype.DistributedLock
import kotlinx.coroutines.sync.Mutex

class MutexDistributedLock : DistributedLock {

    private val lock: Mutex = Mutex()

    override suspend fun isLocked(): Boolean = lock.isLocked

    override suspend fun lock() = lock.lock()

    override suspend fun unlock() = lock.unlock()
}
