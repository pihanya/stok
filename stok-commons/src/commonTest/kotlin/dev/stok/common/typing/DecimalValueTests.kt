@file:Suppress("ktlint:standard:trailing-comma-on-call-site")

package dev.stok.common.typing

import dev.stok.common.kotlin.extension.toDecimal
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.core.spec.style.scopes.ShouldSpecContainerScope
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe

class DecimalValueTests : ShouldSpec({
    context("sqrt() function") {

        data class SqrtTestCase(val value: DecimalValue, val expected: DecimalValue)

        fun doTest(value: DecimalValue, expected: DecimalValue) = value.sqrt() shouldBe expected

        suspend fun ShouldSpecContainerScope.withData(vararg cases: SqrtTestCase) = withData(
            nameFn = { (value, expected) -> "sqrt($value) should be $expected" },
            test = { (value, expected) -> doTest(value, expected) },
            ts = cases.toList(),
        )

        context("work for small integers that give integer sqrt") {
            withData(
                SqrtTestCase(DecimalValue.ZERO, DecimalValue.ZERO),
                SqrtTestCase(DecimalValue.ONE, DecimalValue.ONE),
                SqrtTestCase(4.toDecimal(), 2.toDecimal()),
                SqrtTestCase(9.toDecimal(), 3.toDecimal()),
                SqrtTestCase(16.toDecimal(), 4.toDecimal()),
                SqrtTestCase(25.toDecimal(), 5.toDecimal()),
            )
        }

        context("work for small integers that give decimal sqrt") {
            withData(
                SqrtTestCase(
                    5.toDecimal(),
                    "2.236067977499789696409173668731276".toDecimal()
                ),
                SqrtTestCase(
                    DecimalValue.TEN,
                    "3.162277660168379331998893544432719".toDecimal()
                ),
                SqrtTestCase(
                    11.toDecimal(),
                    "3.316624790355399849114932736670687".toDecimal()
                ),
                SqrtTestCase(
                    17.toDecimal(),
                    "4.123105625617660549821409855974077".toDecimal()
                ),
                SqrtTestCase(
                    27.toDecimal(),
                    "5.196152422706631880582339024517617".toDecimal()
                ),
            )
        }
    }

    context("plus() binary operator") {

        data class PlusTestCase(val left: DecimalValue, val right: DecimalValue, val expected: DecimalValue)

        fun doTest(left: DecimalValue, right: DecimalValue, expected: DecimalValue) =
            left.plus(right) shouldBe expected

        suspend fun ShouldSpecContainerScope.withData(vararg cases: PlusTestCase) = withData(
            nameFn = { (left, right, expected) -> "$left + $right should be $expected" },
            test = { (left, right, expected) -> doTest(left, right, expected) },
            ts = cases.toList(),
        )

        context("works for small integers") {
            withData(
                PlusTestCase(DecimalValue.ONE, DecimalValue.ZERO, DecimalValue.ONE),
                PlusTestCase(DecimalValue.TEN, DecimalValue.ONE, 11.toDecimal()),
                PlusTestCase(
                    1.toDecimal(),
                    2.toDecimal(),
                    3.toDecimal()
                ),
                PlusTestCase(
                    10.toDecimal(),
                    20.toDecimal(),
                    30.toDecimal()
                ),
                PlusTestCase(
                    123.toDecimal(),
                    345.toDecimal(),
                    468.toDecimal()
                ),
            )
        }

        context("works for small floats") {
            withData(
                PlusTestCase(
                    "0.1".toDecimal(),
                    "0.2".toDecimal(),
                    "0.3".toDecimal()
                ),
            )
        }
    }

    context("minus() binary operator") {

        data class MinusTestCase(val left: DecimalValue, val right: DecimalValue, val expected: DecimalValue)

        fun doTest(left: DecimalValue, right: DecimalValue, expected: DecimalValue) =
            left.minus(right) shouldBe expected

        suspend fun ShouldSpecContainerScope.withData(vararg cases: MinusTestCase) = withData(
            nameFn = { (left, right, expected) -> "$left - $right should be $expected" },
            test = { (left, right, expected) -> doTest(left, right, expected) },
            ts = cases.toList(),
        )

        context("works for small integers") {
            withData(
                MinusTestCase(DecimalValue.ONE, DecimalValue.ZERO, DecimalValue.ONE),
                MinusTestCase(DecimalValue.TEN, DecimalValue.ONE, 9.toDecimal()),
                MinusTestCase(
                    345.toDecimal(),
                    123.toDecimal(),
                    222.toDecimal()
                ),
                MinusTestCase(
                    345.toDecimal(),
                    344.toDecimal(),
                    DecimalValue.ONE
                ),
            )
        }

        context("works for small floats") {
            withData(
                MinusTestCase(
                    "0.3".toDecimal(),
                    "0.2".toDecimal(),
                    "0.1".toDecimal()
                ),
                MinusTestCase(
                    "0.999".toDecimal(),
                    "0.888".toDecimal(),
                    "0.111".toDecimal()
                ),
            )
        }
    }
})
