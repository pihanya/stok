package dev.stok.common.kotlinx.coroutines

import io.kotest.assertions.async.shouldTimeout
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.framework.concurrency.continually
import io.kotest.framework.concurrency.eventually
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.booleans.shouldNotBeTrue
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class ScopeCountdownLatchTests : ShouldSpec({

    fun newScope() = CoroutineScope(Dispatchers.Default)

    suspend fun withScope(block: suspend (scope: CoroutineScope) -> Unit) {
        val scope: CoroutineScope = newScope()
        try {
            block(scope)
        } finally {
            if (scope.isActive) {
                scope.cancel()
            }
        }
    }

    should("should await until countdown") {
        withScope { scope ->
            val latch = ScopeCountdownLatch(1, scope)

            launch {
                delay(1.seconds)
                latch.countDown()
            }

            continually(900.milliseconds) {
                shouldTimeout(100.milliseconds) {
                    latch.await()
                }
            }

            delay(100.milliseconds)
            shouldTimeout(10.milliseconds) { latch.await() }
        }
    }

    should("should trigger onCompletion { ... }") {
        withScope { scope ->
            val latch = ScopeCountdownLatch(1, scope)

            val deferred = CompletableDeferred<Unit>()
            latch.onCompletion { deferred.complete(Unit) }

            launch {
                delay(1.seconds)
                latch.countDown()
            }

            continually(800.milliseconds) { deferred.isCompleted.shouldNotBeTrue() }
            eventually(1.seconds) { deferred.isCompleted.shouldBeTrue() }
        }
    }

    should("work normally on multiple coroutines counting down") {
        val coroutinesCount = 1000
        withScope { scope ->
            val latch = ScopeCountdownLatch(coroutinesCount, scope)
            repeat(coroutinesCount) {
                launch {
                    delay(1.seconds)
                    latch.countDown()
                }
            }

            continually(900.milliseconds) {
                shouldTimeout(100.milliseconds) { latch.await() }
            }

            delay(100.milliseconds)
            shouldTimeout(10.milliseconds) { latch.await() }
        }
    }
})
