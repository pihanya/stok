plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                api(libs.benasherUuid)
                implementation(libs.korlibs.kbignum)
            }
        }
    }
}
