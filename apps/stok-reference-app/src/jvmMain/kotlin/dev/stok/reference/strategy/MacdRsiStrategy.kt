package dev.stok.reference.strategy

import dev.stok.CandleIntervals
import dev.stok.StokStrategy
import dev.stok.common.kotlin.extension.times
import dev.stok.context.ParametersAwareContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.parameter
import dev.stok.dsl.strategy
import dev.stok.indicator.getCurrent
import dev.stok.integration.macd
import dev.stok.integration.price
import dev.stok.integration.rsi
import dev.stok.model.StokAccount
import dev.stok.typing.InstrumentId
import kotlin.time.Duration.Companion.minutes

private val ParametersAwareContext.instrument: InstrumentId by parameter("instrumentId")
private val ParametersAwareContext.buyRatio: Double by parameter("buyRatio")

private val ParametersAwareContext.macdhThreshold: Double by parameter("macdhThreshold")
private val ParametersAwareContext.rsiOversoldTrigger: Int by parameter("rsiOversoldTrigger")
private val ParametersAwareContext.rsiOverboughtTrigger: Int by parameter("rsiOverboughtTrigger")

val macdRsiStrategy: StokStrategy = strategy {
    everyPeriod(1.minutes, triggerOnStart = true) {
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
        if (shouldSell(instrument)) {
            sellInstrument(instrument)
        }
    }
}

context(StrategyContext) private suspend fun shouldBuy(instrumentId: InstrumentId): Boolean {
    if (getInstrumentPositionOrNull(instrumentId) != null) return false

    val rsi = indicators.rsi(instrumentId, CandleIntervals.INTERVAL_1DAY).also { logger.info { "RSI: $it" } }
    val macd = indicators.macd(instrumentId, CandleIntervals.INTERVAL_1DAY).also { logger.info { "MACD: $it" } }

    val rsiOversoldSignal = rsi.getCurrent().value?.let { it <= rsiOversoldTrigger } ?: false
    val macdIncreaseTrendStartSignal = macd.getCurrent().value?.histogram?.let { it >= macdhThreshold } ?: false
    return rsiOversoldSignal && macdIncreaseTrendStartSignal
}

context(StrategyContext) private suspend fun shouldSell(instrumentId: InstrumentId): Boolean {
    if (getInstrumentPositionOrNull(instrumentId) == null) return false

    val rsi = indicators.rsi(instrumentId, CandleIntervals.INTERVAL_1DAY).also { logger.info { "RSI: $it" } }
    val macd = indicators.macd(instrumentId, CandleIntervals.INTERVAL_1DAY).also { logger.info { "MACD: $it" } }

    val rsiOverboughtSignal = rsi.getCurrent().value?.let { it >= rsiOverboughtTrigger } ?: false
    val macdDecreaseTrendStartSignal = macd.getCurrent().value?.let { it.histogram <= -macdhThreshold } ?: false
    return rsiOverboughtSignal && macdDecreaseTrendStartSignal
}

context(StrategyContext) private suspend fun buyInstrument(instrument: InstrumentId) {
    val instrumentCurrency = instruments.getInstrumentInfo(instrument).currency
    val availableMoney = run {
        val money = portfolio.getMoney(currency = instrumentCurrency, account = mainAccount())
        money.amount * buyRatio
    }
    val currentPrice = indicators.price(instrument, CandleIntervals.INTERVAL_5MIN).getCurrent().value ?: return

    val quantityToBuy = (availableMoney / currentPrice).toDouble().toLong()
    broker.marketBuy(instrumentId = instrument, quantity = quantityToBuy, account = mainAccount())
}

context(StrategyContext)
private suspend fun sellInstrument(instrument: InstrumentId) {
    val openedPosition = getInstrumentPositionOrNull(instrument)
    if (openedPosition != null) {
        broker.marketSell(openedPosition.instrumentId, openedPosition.lots, account = mainAccount())
    }
}

context(StrategyContext)
private suspend fun getInstrumentPositionOrNull(instrument: InstrumentId) =
    portfolio.getPositions(mainAccount()).firstOrNull { it.instrumentId == instrument }

context(StrategyContext)
private suspend fun mainAccount(): StokAccount =
    portfolio.getAccounts().first()
