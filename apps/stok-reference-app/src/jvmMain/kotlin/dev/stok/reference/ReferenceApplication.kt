package dev.stok.reference

import dev.stok.StokBootstrap
import dev.stok.StokStrategy
import dev.stok.StokStrategyManager
import dev.stok.dsl.manageOn
import dev.stok.reference.strategy.mvpStrategy
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

@Suppress("unused")
class ReferenceApplication

fun main(args: Array<String>) {
    // Step 1. Initializing bootstrap
    val bootstrap: StokBootstrap = tinvestBootstrap

    // Step 2. Defining trading strategy
    val strategy: StokStrategy = mvpStrategy

    // Step 3. Starting trading strategy
    val manager: StokStrategyManager = strategy manageOn bootstrap

    manager.start(wait = true)
    sleepFor(1.minutes)
    manager.shutdown(wait = true)
}

private fun sleepFor(duration: Duration) {
    repeat(60 * duration.inWholeMinutes.toInt()) {
        Thread.sleep(1.seconds.inWholeMilliseconds)
    }
}
