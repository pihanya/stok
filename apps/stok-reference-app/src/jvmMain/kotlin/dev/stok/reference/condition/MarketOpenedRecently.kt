package dev.stok.reference.condition

import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.stereotype.StokCondition
import dev.stok.reference.enum.MarketType
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalTime
import kotlinx.datetime.atTime
import kotlinx.datetime.toInstant
import kotlinx.datetime.toLocalDateTime
import java.time.DayOfWeek
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes

class MarketOpenedRecently(
    private val market: MarketType,
    private val timeWindow: Duration = 5.minutes
) : StokCondition {

    override fun isSatisfied(context: StrategyContext): Boolean {
        val currentTime = context.currentTime
        val openTime = currentDayOpenTime(currentTime) ?: return false
        return (currentTime - openTime <= timeWindow)
    }

    private fun currentDayOpenTime(currentTime: Instant): Instant? = when (market) {
        // https://www.tradinghours.com/markets/nasdaq
        MarketType.NASDAQ -> {
            val localDateTime = currentTime.toLocalDateTime(market.timeZone)
            if (localDateTime.dayOfWeek <= DayOfWeek.FRIDAY) {
                localDateTime.date
                    .atTime(LocalTime(hour = 9, minute = 30))
                    .toInstant(market.timeZone)
            } else {
                null
            }
        }

        // https://www.tradinghours.com/markets/nyse
        MarketType.NYSE -> {
            val localDateTime = currentTime.toLocalDateTime(market.timeZone)
            if (localDateTime.dayOfWeek <= DayOfWeek.FRIDAY) {
                localDateTime.date
                    .atTime(LocalTime(hour = 9, minute = 30))
                    .toInstant(market.timeZone)
            } else {
                null
            }
        }

        // https://www.moex.com/ru/tradingcalendar/
        // https://www.tradinghours.com/markets/moex
        MarketType.MOEX -> {
            val localDateTime = currentTime.toLocalDateTime(market.timeZone)
            if (localDateTime.dayOfWeek <= DayOfWeek.FRIDAY) {
                localDateTime.date
                    .atTime(LocalTime(hour = 9, minute = 50))
                    .toInstant(market.timeZone)
            } else {
                null
            }
        }
    }
}
