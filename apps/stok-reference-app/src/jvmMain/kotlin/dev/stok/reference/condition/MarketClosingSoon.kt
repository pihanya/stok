package dev.stok.reference.condition

import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.stereotype.StokCondition
import dev.stok.reference.enum.MarketType
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalTime
import kotlinx.datetime.atTime
import kotlinx.datetime.toInstant
import kotlinx.datetime.toLocalDateTime
import java.time.DayOfWeek
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes

class MarketClosingSoon(
    private val market: MarketType,
    private val timeWindow: Duration = 5.minutes
) : StokCondition {

    override fun isSatisfied(context: StrategyContext): Boolean {
        val currentTime = context.currentTime
        val closeTime = currentDayCloseTime(currentTime) ?: return false
        return (closeTime - currentTime <= timeWindow)
    }

    private fun currentDayCloseTime(currentTime: Instant): Instant? = when (market) {
        // https://www.tradinghours.com/markets/nasdaq
        MarketType.NASDAQ -> {
            val localDateTime = currentTime.toLocalDateTime(market.timeZone)
            if (localDateTime.dayOfWeek <= DayOfWeek.FRIDAY) {
                localDateTime.date
                    .atTime(LocalTime(hour = 16, minute = 0))
                    .toInstant(market.timeZone)
            } else {
                null
            }
        }

        // https://www.tradinghours.com/markets/nyse
        MarketType.NYSE -> {
            val localDateTime = currentTime.toLocalDateTime(market.timeZone)
            if (localDateTime.dayOfWeek <= DayOfWeek.FRIDAY) {
                localDateTime.date
                    .atTime(LocalTime(hour = 16, minute = 0))
                    .toInstant(market.timeZone)
            } else {
                null
            }
        }

        // https://www.moex.com/ru/tradingcalendar/
        // https://www.tradinghours.com/markets/moex
        MarketType.MOEX -> {
            val localDateTime = currentTime.toLocalDateTime(market.timeZone)
            if (localDateTime.dayOfWeek <= DayOfWeek.FRIDAY) {
                localDateTime.date
                    .atTime(LocalTime(hour = 18, minute = 40))
                    .toInstant(market.timeZone)
            } else {
                null
            }
        }
    }
}
