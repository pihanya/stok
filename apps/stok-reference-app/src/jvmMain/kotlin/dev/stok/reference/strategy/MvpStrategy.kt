package dev.stok.reference.strategy

import dev.stok.CandleIntervals
import dev.stok.StokStrategy
import dev.stok.context.ParametersAwareContext
import dev.stok.context.VariablesAwareContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.onEvent
import dev.stok.dsl.parameter
import dev.stok.dsl.strategy
import dev.stok.dsl.typing.PeriodicMoment.Companion.minutely
import dev.stok.dsl.variable
import dev.stok.indicator.getCurrent
import dev.stok.integration.event.LifecycleEvent.InterruptEvent
import dev.stok.integration.event.QuoteApiEvent
import dev.stok.integration.macd
import dev.stok.integration.rsi
import dev.stok.model.StokAccount
import dev.stok.model.StokPosition
import dev.stok.reference.condition.MarketClosingSoon
import dev.stok.reference.enum.MarketType.MOEX
import dev.stok.stereotype.StokEvent
import dev.stok.typing.InstrumentId
import dev.stok.typing.MoneyValue
import kotlin.time.Duration.Companion.seconds

private val ParametersAwareContext.minPortfolioMoney: MoneyValue by parameter("stok.minPortfolioMoney")
private val ParametersAwareContext.tradedInstruments: List<InstrumentId> by parameter("stok.tradedInstruments")

private var VariablesAwareContext.quotesReceived: Long by variable("stok.quotesReceived", 0L)

val mvpStrategy: StokStrategy = strategy {
    atMoment(minutely(atSecond = 15)) {
        logger.info { "atMoment(minutely(atSecond = 15))" }
        val instrument = tradedInstruments.random()
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
    }

    everyPeriod(10.seconds, triggerOnStart = true) {
        logger.info { "everyPeriod(10.seconds, triggerOnStart = true)" }
        val instrument = tradedInstruments.random()
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
    }

    onCondition(MarketClosingSoon(MOEX)) { sellEverything() }

    onEvent<QuoteApiEvent> {
        logger.info { "New quote for instrument: ${event.instrumentId}" }
        val instrument = tradedInstruments.random()
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
        quotesReceived += 1
    }

    onEvent<StokEvent> {
        if (event is InterruptEvent) return@onEvent
        logger.info { "Event received: ${event::class.simpleName}" }
    }

    lifecycle {
        beforeStart { logger.info { "Minimal portfolio money: $minPortfolioMoney" } }
        beforeFinish { logger.info { "Final counter value: $quotesReceived" } }
    }
}

context(StrategyContext)
private suspend fun shouldBuy(instrumentId: InstrumentId): Boolean {
    val macd = indicators.macd(instrumentId, CandleIntervals.INTERVAL_1DAY)
    val rsi = indicators.rsi(instrumentId, CandleIntervals.INTERVAL_1DAY)

    val macdValue = macd.getCurrent().value
    val rsiValue = rsi.getCurrent().value
    logger.info {
        buildString {
            appendLine("Indicators:")
            appendLine("    MACD: $macdValue")
            append("    RSI: $rsiValue")
        }
    }

    if ((macdValue == null) || (rsiValue == null)) return false

    return (rsiValue < 30) && (macdValue.macd > macdValue.signal)
}

context(StrategyContext)
private suspend fun buyInstrument(instrumentId: InstrumentId) {
    broker.marketBuy(instrumentId = instrumentId, quantity = 10, account = mainAccount())
}

context(StrategyContext)
private suspend fun sellEverything() {
    val accounts: List<StokAccount> = portfolio.getAccounts()
    val positions: List<StokPosition> = accounts.flatMap { account -> portfolio.getPositions(account) }
    for (position in positions) {
        val instrumentId = position.instrumentId
        val quantity = position.lots
        broker.marketSell(instrumentId, quantity, account = mainAccount())
    }
}

context(StrategyContext)
private suspend fun mainAccount(): StokAccount =
    portfolio.getAccounts().first()
