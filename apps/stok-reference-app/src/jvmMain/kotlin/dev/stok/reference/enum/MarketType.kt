package dev.stok.reference.enum

import kotlinx.datetime.TimeZone

enum class MarketType(val timeZone: TimeZone) {

    NASDAQ(TimeZone.of("GMT-04:00")),

    NYSE(TimeZone.of("GMT-04:00")),

    MOEX(TimeZone.of("GMT+03:00"))
}
