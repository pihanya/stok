# TODO List

Things to be done:
[ ] Choose another cache library (probably [Store][Store-github]) and integrate with it
[ ] Add possibility to track strategy metrics
[ ] Replace manual autowiring with [koin][koin-github]
[ ] Replace DecimalValue with [kotlin-multiplatform-bignum][kotlin-multiplatform-bignum-github]
[ ] Use utilities from [fluid-time][fluid-time-github], [FlowExt][FlowExt-github], [Splitties][Splitties-github]

[koin-github]: https://github.com/InsertKoinIO/koin
[fluid-time-github]: https://github.com/fluidsonic/fluid-time
[kotlin-multiplatform-bignum-github]: https://github.com/ionspin/kotlin-multiplatform-bignum
[FlowExt-github]: https://github.com/hoc081098/FlowExt
[Store-github]: https://github.com/MobileNativeFoundation/Store
[Splitties-github]: https://github.com/LouisCAD/Splitties
